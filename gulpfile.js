var gulp = require('gulp');
var plumber = require('gulp-plumber');
var livereload = require('gulp-livereload');
const fs = require("fs"); // Or 'import fs from "fs";' with ESM
const pathBitrix = (fs.existsSync('../3259404.ru/public/css')) ? '../3259404.ru/public/css' : '../../../3259404.ru/public/css';
const baseAllGeks = (fs.existsSync('../3259404.local')) ? '../3259404.local' : './../../3259404.ru';

var less = require('gulp-less-sourcemap');
var lessGlob = require('gulp-less-glob');
var path = require('path');

gulp.task('less', function () {
    gulp.src('less/**/*.less', {
            base: 'less/'
        })
        .pipe(lessGlob())
        .pipe(less({
            sourceMap: {
                sourceMapRootpath: pathBitrix
            }
        }))
        // .pipe(gulp.dest('../3259404.ru/public/css'))
        .pipe(gulp.dest(function (file) {
            //console.log(file.base+'/css');
            return file.base + '/css';
        }));
});

gulp.task('less-main', function () {
    gulp.src('css/main.less')
        .pipe(lessGlob())
        .pipe(less({
            sourceMap: {
                sourceMapRootpath: pathBitrix
            }
        }))
        // .pipe(gulp.dest('../3259404.ru/public/css'))
        .pipe(gulp.dest('css'));
});

var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
gulp.task('autoprefixer', function () {
    // return gulp.src(geks.path.local.maincss)
    //     //.pipe(plumber())
    // 	.pipe(autoprefixer({
    // 		browsers: ['last 3 versions'],
    // 		cascade: false
    // 	}))
    // 	.pipe(gulp.dest('css'))
    //     .pipe(gulp.dest('../3259404.ru/public/css'));
    //     // .pipe(livereload());
    return gulp.src(geks.path.local.maincss)
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css'))
        .pipe(gulp.dest(pathBitrix));
});
gulp.task('autoprefixer2', function () {
    return gulp.src(geks.path.local.maincss)
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css'));
});

gulp.task('autoprefixerPresta', function () {
    return gulp.src('C:/Users/Eugen/Google/glavzavhoz.ru/themes/pf_golmart/css/*.css')
        .pipe(plumber())
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('C:/Users/Eugen/Google/glavzavhoz.ru/themes/pf_golmart/css'));
});

gulp.task('rem', gulp.parallel('autoprefixer'), function () {
    gulp.src(geks.path.local.maincss)
        .pipe(plumber())
        .pipe(gulp.dest(pathBitrix))
        .pipe(gulp.dest('css'));
});
var concat = require('gulp-concat');
var jsDest = '../3259404.ru/public/js';
gulp.task('scripts', function () {
    return gulp.src(['js/html.sortable.min.js', 'js/plugins.js'])
        .pipe(concat('scripts.js'))
        // .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});
gulp.task('scripts2', function () {
    // return gulp.src(jsFiles)
    return gulp.src(['js/html.sortable.min.js', 'js/plugins.js'])
        .pipe(concat('scripts.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('js'));
});
var basePaths = {
    src: '/',
    dest: 'html/assets/',
};
var paths = {
    images: {
        src: basePaths.src + 'img/ico-svg/',
        dest: basePaths.dest + 'img/'
    },
    sprite: {
        src: basePaths.src + 'img/*',
        svg: 'img/sprite.svg',
        css: basePaths.src + 'css/ico-svg.less'
    },
    templates: {
        src: basePaths.src + 'tpl/'
    }
};
var $ = {
    size: require('gulp-size'),
}
gulp.task('sprite', function () {
    return gulp.src(paths.sprite.src)
        .pipe($.svgSprite({
            shape: {
                spacing: {
                    padding: 5
                }
            },
            mode: {
                css: {
                    dest: "./",
                    layout: "diagonal",
                    sprite: paths.sprite.svg,
                    bust: false,
                    render: {
                        scss: {
                            dest: "css/src/_sprite.less",
                            template: "build/tpl/sprite-template.less"
                        }
                    }
                }
            },
            variables: {
                mapname: "icons"
            }
        }))
        .pipe(gulp.dest(basePaths.dest));
});
var spritesmith = require('gulp.spritesmith');
gulp.task('sprite', function () {
    var spriteData =
        gulp.src('img/ico/*.*+(jpg|jpeg|png)') // путь, откуда берем картинки для спрайта
        .pipe(spritesmith({
            imgName: '../img/ico.png',
            cssName: 'ico.less',
            cssFormat: 'less',
            padding: 2,
            cssVarMap: function (sprite) {
                sprite.name = 'ico-' + sprite.name
            }
        }));
    spriteData.img.pipe(gulp.dest('img/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('css/')); // путь, куда сохраняем стили

    // var spriteNumeric =
    //     gulp.src('img/ico/numeric/*.*+(jpg|jpeg|png)') // путь, откуда берем картинки для спрайта
    //         .pipe(spritesmith({
    //             imgName: '../img/ico-numeric.png',
    //             cssName: 'ico-numeric.less',
    //             cssFormat: 'less',
    //             cssVarMap: function(sprite) {
    //                 sprite.name = 'num-' + sprite.name
    //             }
    //         }));
    // spriteNumeric.img.pipe(gulp.dest('img/')); // путь, куда сохраняем картинку
    // spriteNumeric.css.pipe(gulp.dest('css/')); // путь, куда сохраняем стили
    //
    // var spriteMain =
    //     gulp.src('img/ico/numeric/*.*+(jpg|jpeg|png)') // путь, откуда берем картинки для спрайта
    //         .pipe(spritesmith({
    //             imgName: '../img/ico-numeric.png',
    //             cssName: 'ico-numeric.less',
    //             cssFormat: 'less',
    //             cssVarMap: function(sprite) {
    //                 sprite.name = 'num-' + sprite.name
    //             }
    //         }));
    // spriteNumeric.img.pipe(gulp.dest('img/')); // путь, куда сохраняем картинку
    // spriteNumeric.css.pipe(gulp.dest('css/')); // путь, куда сохраняем стили
});

gulp.task('minify-css', gulp.parallel('autoprefixer'), function () {
    return gulp.src(pathBitrix+'main.css')
        .pipe(gulp.dest(pathBitrix));
});

gulp.task('watch', gulp.parallel('minify-css', 'scripts'), function () {
    livereload.listen();
    gulp.watch('js/*.js', gulp.parallel('scripts'));
    gulp.watch('less/**/*.less', gulp.parallel('less-main'));
    gulp.watch(pathBitrix+'main.css').on('change', livereload.changed);
    gulp.watch('less/**/*.less', gulp.parallel('less'));
    //gulp.watch('css/**/*.css', gulp.parallel('autoprefixer')).on('change', livereload.changed);
});

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});


// geks tasks
// packages
const replace          = require('gulp-replace'),
      nunjucks         = require('gulp-nunjucks'),
      run              = require('gulp-run'),
      webpack          = require('webpack'),
      gutil            = require('gulp-util');
      //ftp              = require('vinyl-ftp');

var geks = {
    path : {
        bx : {
            base     : baseAllGeks,
            localtpl : "/local/templates/master_template",

            get tpl() {
                return this.base + this.localtpl
            },

            get c1() {
                return this.base + "/local/components"
            },

            get c2() {
                return this.base + "/local/templates/master_template/components"
            }
        },

        local : {
            js : 'js/',
            mainjs : 'js/main.js',
            devmainjs : 'js/dev/main.js',
            get scripts() {
                let arr = [
                    'dev/main.js',
                    'nav-new.js',
                    'animate.js',
                    'dynamicTimer.js'
                ], that = this;

                arr.forEach((item, i) => {
                    arr[i] = that.js + item;
                });

                return arr
            },
            css : 'css/',
            maincss: 'css/main.css',
            get styles() {
                let arr = [
                    'main.css',
                    'main.css.map',
                ], that = this;

                arr.forEach((item, i) => {
                    arr[i] = that.css + item;
                });

                return arr
            },
            svg : 'img/ico-svg/',
            get icon() {
                let arr = [
                    '*.svg',
                ], that = this;

                arr.forEach((item, i) => {
                    arr[i] = that.svg + item;
                });

                return arr
            },
        }
    }
}

// anonymous functions

const copy = (src, dest, b) => {
    if (!b) b = ".";

    if (b === 'empty') {
        gulp.src(src)
        .pipe(gulp.dest(dest));
    } else {
        gulp.src(src, {base: b})
        .pipe(gulp.dest(dest));
    }
};

const replaceStr = (src, m, r, dest, done) => {
    gulp.src(src, {base:"."})
    .pipe(replace(m, r))
    .pipe(gulp.dest(dest))
    .on('end', done);
};

// tasks

// 1 bitrix url, 2 layout url.
geks.path.bx.components = {
    [geks.path.bx.c1] : "components/local.components",
    [geks.path.bx.c2] : "components/local.templates.master_template.components"
}

gulp.task('geksCloneComponentsJs', (done) => {
    for (var key in geks.path.bx.components) {
        let from = geks.path.bx.components[key].replace(/\//g, '\\'),
            to = key.replace(/\//g, '\\');
        run('xcopy ' + from + ' ' + to + ' /s /e /y').exec(); // clone string
    }

    done();
})

gulp.task('geksCloneCss', (done) => {
    copy(geks.path.local.styles, geks.path.bx.tpl);

    done();
});

gulp.task('geksCloneIcon', (done) => {
    copy(geks.path.local.icon, geks.path.bx.tpl);

    done();
});

gulp.task('geksCloneMainJs', (done) => {
    replaceStr(geks.path.local.mainjs, /tplPath = '\.'/g, "tplPath = '"+geks.path.bx.localtpl+"'", geks.path.bx.tpl, done);
});

const Autoprefixer = require('gulp-autoprefixer');
const AutoprefixBrowsers = [
    "last 2 version",
    'IE >= 9'
];
const Less = require('gulp-less');
// const cleancss = require('less-plugin-clean-css');

gulp.task('geksLess', (done) => {
    gulp.src(['less/main.less'])
	    .pipe(sourcemaps.init())
        .pipe(Less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(Autoprefixer({
            browsers: AutoprefixBrowsers,
            cascade: false,
            remove: false
        }))
        .pipe(sourcemaps.write('.', {
            sourceRoot: '../less/',
            sourceMappingURLPrefix: 'http://layout325/css'
        }))
        .pipe(gulp.dest('./css'))
        .on('end', done);
});

gulp.task('geksJs', (done) => {
    // output will be built to the 'dist' folder.
    let config = require('./webpack.config.js');
    webpack(config, (err, stats) => {
        if (err) throw new gutil.PluginError("webpack", err);
        stats.toString(config.stats).split('\n').map((line) => {
            gutil.log(gutil.colors.blue("[webpack]"), line);
        });
        done();
    });
});

gulp.task('geksTpl', (done) => {
    gulp.src('templates/!(_)*.html')
    .pipe(nunjucks.compile())
    .pipe(gulp.dest('./html/'))
    .on('end', done);
});

gulp.task('geksClone', gulp.series(gulp.parallel('geksCloneCss', 'geksCloneMainJs', 'geksCloneComponentsJs'), (done) => {
    // other files
    copy(['js/animate.js'], geks.path.bx.tpl);

    done();
}));

// object functions

geks.f = {
    less : (done) => {
        gulp.series('geksLess', 'geksCloneCss');
        done();
    },

    js : (done) => {
        gulp.series('geksJs', 'geksCloneMainJs');
        done();
    },

    tpl : (done) => {
        gulp.series('geksTpl');
        done();
    },

    clone : (done) => {
        gulp.series('geksClone');
        done();
    }
};




var user = 'bitrix';
var pwd = 'n8nV6BNh5263kWhSIe';

var localFiles = [
    // './assets/**/*',
    // './*.ico',
    // './*.js',
    './*.css.map',
    // './*.html'
];

var remoteLocation = 'www/';

function getFtpConnection(){
    return ftp.create({
        host: '146.158.13.166',
        port: 21,
        user: user,
        password: pwd,
        parallel: 5,
        log: gutil.log
    })
}

//deploy to remote server
gulp.task('remote-deploy',function(){
    var conn = getFtpConnection();
    return gulp.src(localFiles, {base: '.', buffer: false})
        .pipe(conn.newer(remoteLocation))
        .pipe(conn.dest(remoteLocation))
})

// global task

gulp.task('geks', gulp.series((done) => {
    // gulp.watch('templates/**/*.html', (done) => {geks.f.tpl(done)});
    // gulp.watch(geks.path.local.devmainjs, (done) => {geks.f.js(done)});
    // gulp.watch('components/**/**/**/**/**/**/*.js', gulp.parallel('geksCloneComponentsJs'));
    // gulp.watch('less/**/*.less', (done) => {geks.f.less(done)});
    gulp.watch('templates/**/*.html', gulp.series('geksTpl'));
    gulp.watch(geks.path.local.scripts, gulp.series('geksJs', 'geksCloneMainJs'));
    gulp.watch('less/**/*.less', gulp.series('geksLess', 'geksCloneCss'));
    gulp.watch(geks.path.local.svg + '*.svg', gulp.series('geksCloneIcon'));
    done();
}));

var tasksGeks = ['geksTpl', gulp.series('geksLess', 'geksCloneCss'), gulp.series('geksJs', 'geksCloneMainJs')];
gulp.task('AllGeks', gulp.series( gulp.parallel(...tasksGeks), 'geks'));