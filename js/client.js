// переменные для подбора картриджей
var userChoise = {
    'brandId': '',
    'typeId': '',
    'seriesId': '',
    'modelId': ''
};
var proxy = '/app/index/ajaxproxyjson';
var API_HOST = 'http://api.3259404.ru';
var variantId = 0;
// ---------------------------------



function setEqualHeight(columns) {
    var maxHeight = 0;
    columns.each(function () {
        if ($(this).height() > maxHeight)
            maxHeight = $(this).height();
    });
    columns.height(maxHeight);
}

function refreshCheckbox() {
    $('.el-checkbox input,.form__checkbox>input').each(checkCheckbox);
}
refreshCheckbox();

function checkCheckbox() {
    if ($(this).prop('checked')) {
        $(this).parent().addClass('active');
    } else {
        $(this).parent().removeClass('active');
    }
    if ($(this).hasClass('form__checkbox-big')) {
        $(this).parent().addClass('form__checkbox_big');
    }
};

function sliderDefault() {
    $('.js-slider-default').each(function () {
        var item = $(this).data('item');
        var gallery = $(this).find('.js-slider-default__gallery');
        var num = gallery.children().length;
        if (num > item) {
            gallery.slick({
                infinite: true,
                dots: false,
                arrows: false,
                slidesToShow: item,
                slidesToScroll: 1
            });
            $(this).find('.js-slider-default__but-wrap').show();
        }
    });
};

//функции авторизации через соц сети
(function ($) {
    //  inspired by DISQUS
    $.oauthpopup = function (options) {
        if (!options || !options.path) {
            throw new Error("options.path must not be empty");
        }
        var screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
            screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
            outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
            outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
            left = parseInt(screenX + ((outerWidth - 800) / 2), 10),
            top = parseInt(screenY + ((outerHeight - 700) / 2.5), 10),
            options = $.extend({
                windowName: 'ConnectWithOAuth' // should not include space for IE
                    ,
                windowOptions: 'location=0,status=0,width=800,height=700,left=' + left + ',top=' + top,
                callback: function () { /*window.location.reload();*/ }
            }, options);

        var oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
        /*var oauthInterval = window.setInterval(function(){
            if (oauthWindow.closed) {
                window.clearInterval(oauthInterval);
                options.callback();
            }
        }, 1000);*/
    };

    //bind to element and pop oauth when clicked
    $.fn.oauthpopup = function (options) {
        $this = $(this);
        $this.click($.oauthpopup.bind(this, options));
    };
})(jQuery);

jQuery(document).ready(function () {
    pagination = {
        item: $('.cart-info__content>.cart-info__item'), //элемент который считаем
        num: 5, //число элементов на странице
        currentPage: 1, //текущая страница
        // hConteiner: function(){
        //         var allH =  0,
        //         counterCondition = this.num*this.currentPage,
        //         counterInit = (this.num - counterCondition)*-1;
        //         for(i = counterInit; i <= counterCondition-1; i++){
        //             allH += $('.cart-info__content>.cart-info__item').eq(i).outerHeight(true);
        //         }
        //         // console.log(this.num, this.item.length);
        //         // $('.cart-info__content').height(allH + ($('.cart-info__content').innerHeight() - $('.cart-info__content').height()) - 10);
        //         var h = allH + ($('.cart-info__content').innerHeight() - $('.cart-info__content').height()) - 10;
        //         $('.cart-info__content').css('min-height',h)
        // },
        all: function () {
            var all = Math.ceil($('.cart-info__content>.cart-info__item').length / this.num);
            return all = (all < 1) ? 1 : all;
        },
        init: function () {
            // console.log('init',this.currentPage,this.all());
            var item = $('.cart-info__content>.cart-info__item');
            if (this.all() < this.currentPage) { //parseInt($('.cart-info__pagination-all').text())
                this.currentPage--;
            }
            $('.cart-info__pagination-cur').html(this.currentPage);
            $('.cart-info__pagination-all').html((this.all() < 1) ? 1 : this.all());

            if (this.currentPage == 1) {
                $('.js-cart-info__prev').hide();
            }
            if (this.currentPage == this.all()) {
                $('.js-cart-info__next').hide();
            }
            item.hide();
            item.slice((this.currentPage - 1) * this.num, this.currentPage * this.num).show();
            // this.hConteiner();

            if (this.all() <= 1) {
                //   $('.js-cart-info__next').hide();
                //   $('.js-cart-info__prev').hide();
                $('.cart-info__pagination').hide();
            }
            if (this.all() > 1) {
                $('.cart-info__content').addClass('cart-info__content_full');
            }
        },
        next: function (el) {
            //   console.log('next',this.currentPage);
            var item = $('.cart-info__content>.cart-info__item');
            if (this.currentPage == this.all() - 1) {
                el.hide();
            }
            if (this.currentPage == 1) {
                el.parents('.cart-info').find('.js-cart-info__prev').show();
            }
            this.currentPage++;
            item.hide();
            item.slice((this.currentPage - 1) * this.num, this.currentPage * this.num).show();
            $('.cart-info__pagination-cur').html(this.currentPage);
        },
        prev: function (el) {
            //   console.log('prev',this.currentPage);
            var item = $('.cart-info__content>.cart-info__item');
            if (this.currentPage == 2) {
                el.hide();
            }
            if (this.currentPage == this.all()) {
                el.parents('.cart-info').find('.js-cart-info__next').show();
            }
            this.currentPage--;
            item.hide();
            item.slice((this.currentPage - 1) * this.num, this.currentPage * this.num).show();
            $('.cart-info__pagination-cur').html(this.currentPage);
        }
    };

    $(document).on('click', '.js-cart-info__next', function () {
        pagination.next($(this));
    });
    $(document).on('click', '.js-cart-info__prev', function () {
        pagination.prev($(this));
    });

    //    slick gallery
    sliderDefault();
    $(document).on('click', '.js-recent-variants-next', function () {
        $('.js-recent-variants').slick('slickNext');
    });
    $(document).on('click', '.js-recent-variants-prev', function () {
        $('.js-recent-variants').slick('slickPrev');
    });

    $(document).on('click', '.js-slider-default__next', function () {
        $(this).parents('.js-slider-default').find('.js-slider-default__gallery').slick('slickNext');
    });
    $(document).on('click', '.js-slider-default__prev', function () {
        $(this).parents('.js-slider-default').find('.js-slider-default__gallery').slick('slickPrev');
    });
    // Похожие товары REES46
    // if (variantId != 0){
    // r46('track', 'view', variantId);
    // r46('recommend', 'similar', {item: variantId, cart: [cartVariantIds], category: [variantParentGroupId]}, function(ids) {
    // if (ids.length > 0)
    // $.ajax({
    // type: "POST",
    // url: "/catalog/index/items",
    // data: { //переменные
    // variantIds: ids,
    // hidefavorite: true,
    // hidevariants: true,
    // hidespecprice: true
    // },
    // dataType: "html",
    // success: function (data) {
    // $('.js-goods-like-block').html(data);
    // $('#variant__goods-like').show();
    // jsInputSpinRefresh();
    // sliderDefault();
    // }
    // });
    // });
    // }

    //setEqualHeight($("#catalog-menu-line .menu-item .wrapper .inner"));

    // //узкая версия выпад меню
    // jQuery(this).on('click', ".js-display-catalog-menu", function(){
    //     jQuery("nav#catalog-menu").toggle('slide', {direction: 'left'}, 500);
    // });

    //ajax загрузка виджета корзины
    // jQuery(".js-ajax-load-cart-content").load(loadCartContent())
    // .find(".cart-content-block").show();
    //показ виджета корзины
    jQuery(".js-ajax-load-cart-content").on('click', function () {
        if (jQuery(this).attr('sync') == 'false') {
            loadCartContent();
        }
    });

    //показ менюшки ЛК
    jQuery(".js-show-lk-menu").hover(function () {
        jQuery(".js-show-lk-menu .lk-menu-block").show();
    }, function () {
        jQuery(".js-show-lk-menu .lk-menu-block").hide();
    });

    //ajax добавление в корзину
    jQuery(this).on('click', ".js-ajax-addtocart", function () {
        var XY = jQuery(this).offset();
        var clicked = jQuery(this);
        var varId = jQuery(this).attr('variantid');
        var count = jQuery(this).parents('.js-ajax-addtocart-wrap').find('.js-addtocart-count').val();
        r46('track', 'cart', varId); // REES46 добавл в корзину
        jQuery.ajax({
            type: "POST",
            url: "/cart/index/addtocart",
            data: {
                variantId: varId,
                count: (count != null ? count : 1)
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                if (data.success) {
                    jQuery(".js-ajax-load-cart-content").attr('sync', 'false');

                    jQuery(".js-ajax-load-cart-content .cart-summ").text(data.cartTotalSumm + ' руб.');
                    // jQuery(".js-ajax-load-cart-content .cart-count").text(data.cartTotalCount);
                    if (data.cartTotalCount > 0) {
                        jQuery(".js-ajax-load-cart-content").addClass('header__but_cart-full');
                        jQuery(".js-ajax-load-cart-content").removeClass('js-disabled');
                    } else {
                        jQuery(".js-ajax-load-cart-content").removeClass('header__but_cart-full');
                        jQuery(".js-ajax-load-cart-content").addClass('js-disabled');
                    }
                    jQuery(".js-ajax-load-cart-content").attr('data-count', data.cartTotalCount);
                    flashMessenger('success', [data.html], 5000);
                    loadCartContent();

                    // MIX Market
                    if (typeof isProduction != 'undefined' && isProduction == 1)
                        jQuery('body').append('<img src="http://mixmarket.biz/uni/tev.php?id=1294936163&r=' + escape(document.referrer) + '&t=' + (new Date()).getTime() + '" width="1" height="1"/>');
                } else {
                    flashMessenger('warning', [data.message], 5000);
                }
            }
        });
    });

    //ajax удаление из корзины
    jQuery(this).on('click', ".js-ajax-removefromcart", function () {
        var XY = jQuery(this).offset();
        var clicked = jQuery(this);
        var varId = jQuery(this).attr('variantid');
        r46('track', 'remove_from_cart', varId); // REES46 удаление из корзины
        jQuery.ajax({
            type: "POST",
            url: "/cart/index/removefromcart",
            data: {
                variantId: varId
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                jQuery('.js-cart-total-summ-block').html(data.cartTotalSumm).attr('summ', data.cartTotalSummFloat);
                jQuery('.js-cart-total-source-summ-block').html(data.cartTotalSourceSumm).attr('summ', data.cartTotalSourceSummFloat);
                jQuery('.js-cart-total-assemble-summ-block').html(data.cartTotalAssembleSumm).attr('summ', data.cartTotalAssembleSummFloat);
                jQuery('.js-cart-benefit-summ-block').html(data.cartBenefitSumm);

                var del = [clicked.parents('#cart-item-variant-id-' + varId), $('.cart-item-variant-idhelper-' + varId)];
                $($.map(del, function (el) {
                    return $.makeArray(el)
                })).remove();

                if (data.html != undefined) {
                    flashMessenger('success', [data.html], 5000);
                } else {
                    flashMessenger('success', ['Товар был удален из корзины'], 5000);
                }

                if (jQuery("[id|='cart-item-variant-id']:visible").length == 0) { //если не осталось товаров в корзине
                    jQuery("#ItemsOptions, .cart-footer").remove();
                    jQuery(".cart-empty").show();
                }
                loadCartContent();
                refreshTable();
                slideTopDown();
                checkMinSumm(data.cartTotalSummFloat);
                checkSummInBlockAndShowOrHide(data.cartBenefitSummFloat, '.js-benefit-block');
                checkSummInBlockAndShowOrHide(data.cartTotalAssembleSummFloat, '.js-assemble-block');
            }
        });
    });

    //ajax обновление количества в корзине
    var ajaxUpdateCartItemCount = null;
    jQuery(this).on('change', ".js-ajax-update-cartitem-count", function () {
        if (ajaxUpdateCartItemCount != null)
            ajaxUpdateCartItemCount.abort();
        var count = jQuery(this).val();
        var varId = jQuery(this).attr('variantid');
        var cartItemId = jQuery(this).attr('cartitemid');
        ajaxUpdateCartItemCount = jQuery.ajax({
            type: "POST",
            url: "/cart/index/updateitemcount",
            data: {
                count: count,
                variantId: varId,
                cartItemId: cartItemId
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                if (data.success == true) {
                    jQuery('.js-cart-total-summ-block').html(data.cartTotalSumm).attr('summ', data.cartTotalSummFloat);
                    jQuery('.js-cart-total-source-summ-block').html(data.cartTotalSourceSumm).attr('summ', data.cartTotalSourceSummFloat);
                    jQuery('.js-cart-benefit-summ-block').html(data.cartBenefitSumm);
                    jQuery('.js-cart-total-assemble-summ-block').html(data.cartTotalAssembleSumm).attr('summ', data.cartTotalAssembleSummFloat);
                    jQuery('#cart-item-variant-id-' + varId + ' .js-cart-item-summ').html(data.cartItemSumm);
                    jQuery('.js-cartItemSummInfo').html(data.cartItemSummInfo);
                    checkMinSumm(data.cartTotalSummFloat);
                    checkSummInBlockAndShowOrHide(data.cartBenefitSummFloat, '.js-benefit-block');
                    slideTopDown();
                    loadCartContent();
                }
            }
        });
    });

    //ajax обновление сборки в корзине
    var ajaxUpdateCartItemAssemble = null;
    jQuery(this).on('change', ".js-ajax-update-cartitem-assemble", function () {
        if (ajaxUpdateCartItemAssemble != null)
            ajaxUpdateCartItemAssemble.abort();
        var assemble = jQuery(this).val();
        var varId = jQuery(this).attr('variantid');
        var cartItemId = jQuery(this).attr('cartitemid');
        ajaxUpdateCartItemAssemble = jQuery.ajax({
            type: "POST",
            url: "/cart/index/updateitemassemble",
            data: {
                assemble: assemble,
                variantId: varId,
                cartItemId: cartItemId
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                if (data.success == true) {
                    jQuery('.js-cart-total-summ-block').html(data.cartTotalSumm).attr('summ', data.cartTotalSummFloat);
                    jQuery('.js-cart-total-assemble-summ-block').html(data.cartTotalAssembleSumm).attr('summ', data.cartTotalAssembleSummFloat);
                    checkSummInBlockAndShowOrHide(data.cartTotalAssembleSummFloat, '.js-assemble-block');
                    slideTopDown();
                    loadCartContent();
                }
            }
        });
    });

    function checkMinSumm(summ) {
        if (orderMinSumm !== undefined) {
            if (summ >= orderMinSumm || jQuery('.js-addtoorder-checkbox').is(':checked')) {
                jQuery('.js-cart-submit').addClass('el-button_prim-0').removeClass('cart-widget__but_disabled el-button_scnd-1');
                jQuery('.js-order-min-summ-notify, .js-order-is-add-to-order-block, .js-suggest-to-add-summ-notify').hide();
            } else {
                jQuery('.js-cart-submit').removeClass('el-button_prim-0').addClass('cart-widget__but_disabled el-button_scnd-1') /*.hide()*/ ;
                jQuery('.js-order-min-summ-notify, .js-order-is-add-to-order-block').show();
            }
        }
    }

    function checkSummInBlockAndShowOrHide(summ, selector) {
        if (summ > 0)
            jQuery(selector).show();
        else
            jQuery(selector).hide();
    }

    //ajax добавление в вишлист / закладки
    jQuery(this).on('click', ".js-ajax-addtowishlist", function () {
        var XY = jQuery(this).offset();
        var clicked = jQuery(this);
        var wishlistId = jQuery(this).attr('wishlistid');
        var varId = jQuery(this).attr('variantid');
        var count = jQuery(this).attr('count');
        jQuery.ajax({
            type: "POST",
            url: "/wishlist/index/addtowishlist",
            data: {
                wishlistId: wishlistId,
                variantId: varId,
                count: count
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                flashMessenger('success', [data.html], 5000);
                if (clicked.parent().hasClass('js-wishlist-actions-block')) {
                    clicked.addClass('js-ajax-removefromwishlist')
                        .removeClass('js-ajax-addtowishlist')
                        .addClass('active-all')
                        .children('span').text('В закладках');
                }

                clicked.next('.js-ajax-removefromwishlist').show();


                if (clicked.parent().hasClass('js-wishlist-outer-container') && data.actions != undefined) {
                    var position = clicked.offset();
                    $('.js-wishlist-outer-container__trg').html(data.actions).css({
                        'left': position.left,
                        'top': position.top - 1
                    });
                    $('.js-ajax-variantwishlists,.js-ajax-addtowishlist,.js-wishlist-outer-container > .js-ajax-removefromwishlist').show();
                    clicked.hide();
                } else {
                    //console.log( 'position');
                    clicked.parent('.js-wishlist-actions-block').html(data.actions);
                }

            }
        });
    });

    //ajax удаление из вишлиста / закладки
    jQuery(this).on('click', ".js-ajax-removefromwishlist", function () {
        var clicked = jQuery(this);
        var variantId = jQuery(this).attr('variantid');
        var itemId = jQuery(this).attr('itemid');

        var quest = jQuery(this).attr('confirm');
        if (quest != 'false') {
            if (confirm(quest ? quest : 'Вы уверены?') == false)
                return false;
        }

        jQuery.ajax({
            type: "POST",
            url: '/wishlist/index/removefromwishlist',
            data: {
                'itemid': itemId,
                'variantid': variantId
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {},
            success: function (data) {
                flashMessenger('success', ['Товар успешно удален из списка.'], 5000);

                if (clicked.parent().hasClass('list-unwrap__item'))
                    clicked.hide();
                else
                    clicked.addClass('js-ajax-addtowishlist')
                    .removeClass('js-ajax-removefromwishlist')
                    .removeClass('active-all')
                    .children('span').text('В закладки');


                if (clicked.parent().hasClass('js-wishlist-outer-container') && data.actions != undefined) {
                    var position = clicked.offset();
                    $('.js-wishlist-outer-container__trg').html(data.actions).css({
                        'left': position.left,
                        'top': position.top - 1
                    });
                    $('.js-ajax-variantwishlists,.js-ajax-addtowishlist,.js-wishlist-outer-container > .js-ajax-removefromwishlist').show();
                    clicked.hide();
                } else {
                    clicked.parent('.js-wishlist-actions-block').html(data.actions);
                }
            }
        });
        return false;
    });

    //ajax вишлисты товара
    jQuery(this).on('click', ".js-ajax-variantwishlists", function () {
        var clicked = jQuery(this);
        var varId = jQuery(this).attr('variantid');
        var count = jQuery(this).attr('count');
        jQuery.ajax({
            type: "POST",
            url: "/wishlist/index/variantwishlists",
            data: {
                variantId: varId,
                count: count,
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {

                if (data.actions != '') {

                    if (clicked.parent().hasClass('js-wishlist-outer-container') && data.actions != undefined) {
                        var position = clicked.offset();
                        $('.js-wishlist-outer-container__trg').html(data.actions).css({
                            'left': position.left,
                            'top': position.top - 1
                        });
                        $('.js-ajax-variantwishlists,.js-ajax-addtowishlist,.js-wishlist-outer-container > .js-ajax-removefromwishlist').show();
                        clicked.hide();
                    } else {
                        clicked.parent('.js-wishlist-actions-block').html(data.actions);
                    }

                }

            }
        });
    });







    //ajax обновление количества в вишлисте
    var ajaxUpdateWishlistItemCount = null;
    jQuery(this).on('change', ".js-ajax-update-wishlistitem-count", function () {
        if (ajaxUpdateWishlistItemCount != null)
            ajaxUpdateWishlistItemCount.abort();
        var count = jQuery(this).val();
        var varId = jQuery(this).attr('variantid');
        var wishlistId = jQuery(this).attr('wishlistid');
        var wishlistItemId = jQuery(this).attr('wishlistitemid');
        ajaxUpdateWishlistItemCount = jQuery.ajax({
            type: "POST",
            url: "/wishlist/index/updateitemcount",
            data: {
                count: count,
                variantId: varId,
                wishlistId: wishlistId,
                wishlistItemId: wishlistItemId
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                if (data.success == true) {
                    var wishlistId = data.wishlistId;
                    var liBlock = jQuery('div[wishlistid=' + wishlistId + ']').parent();
                    liBlock.find('.js-wishlistitems-summ').text(data.wishlistTotalSumm);
                    liBlock.find('.js-wishlistitems-count').text(data.wishlistTotalCount);
                }
            }
        });
    });

    //ajax загрузка товаров группы
    jQuery(this).on('click', ".js-ajax-loadmoreitems", function () {
        var clicked = jQuery(this);
        var href = jQuery(this).attr('href');
        jQuery.ajax({
            type: "GET",
            url: href,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                jQuery('.catalog-items-block').addClass('ajax-error').html('Произошла ошибка. Пожалуйста, перезагрузите страницу'); //'Во время AJAX запроса произошла ошибка: '+textStatus+'!'
            },
            success: function (data) {
                clicked.parents('.catalog-moreitems-block').remove();
                jQuery('.catalog-items-block').removeClass('ajax-error').append(data.html);
                jsInputSpinRefresh();
            }
        });
        return false;
    });

    $(document).on('click', '.disabled', function (event) {
        event.preventDefault();
        /* Act on the event */
        return false;
    });
    //фильтры
    jQuery(this).on('click', ".js-ajax-loadfilters", function (e) {
        e.preventDefault();
        var clicked = jQuery(this);
        var href = jQuery(this).attr('href');
        if (href == '#')
            href = window.atob(jQuery(this).data('href'));
        var alias = jQuery(this).attr('alias');
        //jQuery('.catalog-filter-block').html(loaderTemplate);
        jQuery.ajax({
            type: "GET",
            url: href,
            data: {
                load: 'filters',
                alias: alias
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                jQuery('.catalog-filter-block').addClass('ajax-error').html('Произошла ошибка. Пожалуйста, перезагрузите страницу'); //'Во время AJAX запроса произошла ошибка: '+textStatus+'!'
            },
            success: function (data) {
                //обновление фильтра
                jQuery('.catalog-filter-block').removeClass('ajax-error').html(data.filter);
                sliderRangeRefresh();
                refreshCheckbox();
                // jQuery('.js-filter-'+alias).append( jQuery('.js-filteritems-button').html() );
                // console.log(clicked.length);
                // clicked.append( jQuery('.js-filteritems-button').html() );
                jQuery('.js-filter-' + alias + ' a').eq(0).append(jQuery('.js-filteritems-button').html());
            }
        });

        //return false;
    });

    //показать товары по фильтрам
    jQuery(this).on('click', ".js-ajax-loadfilteritems", function () {
        var clicked = jQuery(this);
        var href = jQuery(this).attr('href');
        if (href == '#')
            href = window.atob(jQuery(this).data('href'));
        jQuery('.js-catalog-items').html(loaderTemplate);
        jQuery(this).parents('.filters-show').remove();
        $('html, body').animate({
            scrollTop: $('#goods-list').offset().top - 260
        }, 'fast');
        jQuery.ajax({
            type: "GET",
            url: href,
            data: {
                load: 'items'
            },
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                jQuery('.js-catalog-items').addClass('ajax-error').html('Произошла ошибка. Пожалуйста, перезагрузите страницу');
            },
            success: function (data) {
                //запись в историю
                if (href != window.location) {
                    window.history.pushState(null, null, href);
                }
                //подгрузка товаров по фильтру
                jQuery('.js-catalog-items').removeClass('ajax-error').html(data.items);
                jsInputSpinRefresh();
            }
        });

        return false;
    });



    //вспомнить зачем это нужно
    $(window).bind('popstate', function () {
        window.location.reload("true");
    });

    // обратная связь скрытие/показ полей о заказе
    if (jQuery('.js-theme-selector').length) {
        var sel = jQuery('.js-theme-selector').val().split('. ');
        if (sel[0] == '1' || sel[0] == '3')
            jQuery('.form_feedback .js-about-order').show();

        jQuery('.js-theme-selector').change(function () {
            sel = jQuery(this).val().split('. ');
            if (sel[0] == '1' || sel[0] == '3') {
                jQuery('.form_feedback .js-about-order').show();
            } else {
                jQuery('.form_feedback .js-about-order').hide();
            }
        });
    }

    // предложения поиска
    var ajaxLoadSuggestSearch = null;
    jQuery(this).on('keyup', '.js-ajax-search-suggest', function (I) {
        if (I.key.length > 1 && (jQuery.inArray(I.keyCode, [8, 46]) == -1)) //если alt shift и тп исключая del bksp
            return;

        if (ajaxLoadSuggestSearch != null)
            ajaxLoadSuggestSearch.abort();

        var sugBlock = jQuery(".js-suggest-block");
        var query = jQuery(this).val();
        if (query.length >= 3) {
            ajaxLoadSuggestSearch = jQuery.ajax({
                type: "POST",
                url: "/catalog/search/suggest",
                data: {
                    query: query
                },
                dataType: "json",
                success: function (data) {
                    sugBlock.fadeIn(150);
                    trgmenu.fadeIn(150);
                    sugBlock.html(data.html);
                    $('.header-info').removeClass('header-info_active');
                    //поиск по стрелкам выпадающего списка
                    var arrayLi = [];
                    $('.search-dropdown__section .search-dropdown__item').each(function () {
                        arrayLi.push($(this));
                    });
                    var i = 0,
                        number = '',
                        count = 'undefined' !== typeof arrayLi ? (arrayLi.length - 1) : 0;
                    if (arrayLi[i] != undefined) {
                        $('#keyUpDown').on('keyup', function (event) {
                            //console.log(arrayLi[i]);
                            //event=fixEvent(event);
                            $(this).removeClass('nocaret');
                            if (count != 0) {
                                if (38 == event.keyCode) {
                                    $(this).addClass('nocaret');
                                    event.preventDefault();
                                    this.selectionEnd = $(this).val().length;
                                    this.selectionStart = $(this).val().length;

                                    if (0 < number) {
                                        arrayLi[number].removeClass('active');
                                        --number;
                                        --i;
                                        arrayLi[number].addClass('active');
                                    }
                                } else if (40 == event.keyCode) {
                                    $(this).addClass('nocaret');
                                    event.preventDefault();
                                    this.selectionEnd = $(this).val().length;
                                    this.selectionStart = $(this).val().length;

                                    if (i != count) {
                                        if (arrayLi[i].hasClass('active') && i < count) {
                                            ++i;
                                        }
                                        for (var l in arrayLi) {
                                            if (number == l) {
                                                arrayLi[number].removeClass('active');
                                            }
                                            if (l == i) {
                                                number = i;
                                                arrayLi[i].addClass('active');
                                            }
                                        }
                                    }
                                } else if (27 == event.keyCode) {
                                    sugBlock.fadeOut(150);
                                    trgmenu.fadeOut(150);
                                    closeLightBox();

                                } else if (13 == event.keyCode) {
                                    var list = $('.search-dropdown__section .search-dropdown__item.active').find('.search-dropdown__link').attr('href');
                                    if (list != undefined) {
                                        //console.log(list, 2);
                                        event.preventDefault();
                                        window.location.href = list;
                                    }
                                } else {
                                    return;
                                }
                            }
                        });
                    } //if

                }
            });
        } else {
            sugBlock.fadeOut(150);
            trgmenu.fadeOut(150);
        }
    });
    $('#keyUpDown').on('focus', function (event) {
        $(this).removeClass('nocaret');
    });

    // поиск городов
    var ajaxLoadCitySearch = null;
    jQuery(this).on('keyup', '.js-ajax-search-city', function (I) {
        if (I.key.length > 1 && (jQuery.inArray(I.keyCode, [8, 46]) == -1)) //если alt shift и тп исключая del bksp
            return;

        if (ajaxLoadCitySearch != null)
            ajaxLoadCitySearch.abort();

        var regionsBlock = jQuery(".js-regions");
        var citiesBlock = jQuery(".js-region-cities");
        var query = jQuery(this).val();
        if (query.length >= 3 || query.length == 0) {
            ajaxLoadCitySearch = jQuery.ajax({
                type: "GET",
                url: "/app/dpd/ajaxchoosecity",
                data: {
                    query: query
                },
                dataType: "json",
                success: function (data) {
                    regionsBlock.html(data.regions);
                    citiesBlock.html(data.cities);
                }
            });
        }
    });

    //ajax подгрузка баннеров в меню
    var timer = null;
    var groupIdBanner = [];
    var ajaxLoadMenuBanner = null;
    var leftBannerInit = $('#nav_list').width();
    $('.js-menu-banner-block').css('left', leftBannerInit);

    jQuery(this).on('mouseenter', ".js-ajax-menu-banner", function () {
        var groupId = jQuery(this).attr('groupid');
        var block = jQuery('.js-menu-banner-block');
        if (groupIdBanner[groupId] == null) {
            if (ajaxLoadMenuBanner != null)
                ajaxLoadMenuBanner.abort();
            if (timer != null)
                clearTimeout(timer);
            timer = setTimeout(function () { //тайм аут на загрузку баннера
                ajaxLoadMenuBanner = jQuery.ajax({
                    type: "POST",
                    url: '/banner/index/menubanner',
                    data: {
                        type: 'group_menu',
                        groupId: groupId,
                        count: 1,
                        random: true
                    },
                    dataType: "html",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
                    },
                    success: function (data) {
                        groupIdBanner[groupId] = data;
                        if (data != '')
                            block.html(data);
                    }
                });
            }, 600); //задержка в мс
        } else {
            if (groupIdBanner[groupId] != '')
                block.html(groupIdBanner[groupId]);
        }
        var leftBanner = 0;
        $('#js-menuopen').find('.nav_list:visible').each(function () {
            leftBanner += $(this).width(); //return
        });
        // console.log('in',leftBanner);
        block.show().css('left', leftBanner);

    }).on("mouseleave", "nav,div", function () {
        $('.js-menu-banner-block').fadeOut('fast');
        clearTimeout(timer);
    });

    //скрыть / показать блок .hide_block
    jQuery('.js-hide').click(function () {
        if (jQuery(this).hasClass('next'))
            jQuery(this).nextAll('div.hide_block').addClass('hide');
        else
            jQuery(this).prevAll('div.hide_block').addClass('hide');
        jQuery(this).addClass('hide');
        jQuery('.js-show').removeClass('hide');
    });
    jQuery('.js-show').click(function () {
        if (jQuery(this).hasClass('next'))
            jQuery(this).nextAll('div.hide_block').removeClass('hide');
        else
            jQuery(this).prevAll('div.hide_block').removeClass('hide');
        jQuery(this).addClass('hide');
        jQuery('.js-hide').removeClass('hide');
    });

    //подбор картриджей
    //очищаем все элементы option в select с типами и моделями,делаем эти select disabled
    jQuery('.js-ajax-cartrijes-form #type').attr('disabled', 'disabled');
    jQuery('.js-ajax-cartrijes-form #sendIt').attr('disabled', 'disabled');
    jQuery('.js-ajax-cartrijes-form #type').attr('disabled', 'disabled');
    jQuery('.js-ajax-cartrijes-form #series').attr('disabled', 'disabled');
    jQuery('.js-ajax-cartrijes-form #model').attr('disabled', 'disabled');

    //адрес прокси хендлера

    //загружаем бренды
    if (jQuery('.js-ajax-cartrijes-form').length > 0) {
        jQuery.ajax({
            type: "POST",
            url: proxy,
            dataType: "json",
            data: {
                url: API_HOST + '/rashodnik/user/getbrands'
            },
            success: function (data) {
                options(".js-ajax-cartrijes-form .js-ajax-getBrands", data);
            }
        });
    }

    //по бренду загружаем все
    jQuery('.js-ajax-cartrijes-form .js-ajax-getBrands').on('change', function () {
        userChoise = {
            'brandId': jQuery(this).val(),
            'typeId': '',
            'seriesId': '',
            'modelId': ''
        };
        updateTypesSelect();
        updateSeriesSelect();
        updateModelsSelect();
        updateSubmitButton();
    });

    jQuery('.js-ajax-cartrijes-form #type').on('change', function () {
        userChoise = {
            'brandId': userChoise.brandId,
            'typeId': jQuery(this).val(),
            'seriesId': '',
            'modelId': ''
        };
        updateSeriesSelect();
        updateModelsSelect();
        updateSubmitButton();
    });

    jQuery('.js-ajax-cartrijes-form #series').on('change', function () {
        userChoise = {
            'brandId': userChoise.brandId,
            'typeId': userChoise.typeId,
            'seriesId': jQuery(this).val(),
            'modelId': ''
        };
        updateModelsSelect();
        updateSubmitButton();
    });

    jQuery('.js-ajax-cartrijes-form #model').on('change', function () {
        userChoise = {
            'brandId': userChoise.brandId,
            'typeId': userChoise.typeId,
            'seriesId': userChoise.seriesId,
            'modelId': jQuery(this).val()
        };
        updatePrinterFullName();
        updateSubmitButton();
    });
    // конец подбора картриджей

    //загрузка принтеров к катриджу
    if (jQuery('.js-ajax-load-printers').length > 0) {
        jQuery('.js-ajax-load-printers').each(function () {
            var printersBlock = jQuery(this);
            var longNumber = jQuery(this).attr('longnumber');
            jQuery.ajax({
                type: "POST",
                url: proxy,
                dataType: "json",
                data: {
                    url: API_HOST + '/rashodnik/user/getprintersforlongnumber',
                    longNum: longNumber
                },
                success: function (data) {
                    if (data.length > 0) {
                        jQuery.each(data, function (keyObj, valObj) {
                            printersBlock.append('<li>' + valObj.p_brand + ' ' + valObj.p_series + ' ' + valObj.p_number + ';</li>');
                        });
                    } else {
                        printersBlock.html('Нет данных');
                    }
                }
            });
        });
    }

    // ajax голосовалка за отзыва
    jQuery(this).on('click', ".js-ajax-review-vote", function () {
        var clicked = jQuery(this);
        var data = jQuery(this).data();
        jQuery.ajax({
            type: "POST",
            url: "/review/index/vote",
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                if (data.success == true) {
                    flashMessenger('success', ['Ваш голос учтен, спасибо.'], 5000);
                    clicked.addClass('active');
                    var x = parseInt(clicked.text());
                    clicked.children('span').text(x + 1);
                } else {
                    flashMessenger('warning', [data.error], 5000);
                }
            }
        });
        return false;
    });

    //подсчет подарков
    jQuery('.js-gift-configurator').each(function () {
        gift_configurator(jQuery(this));
    });

    jQuery('.js-gift-configurator input.js-input-spin:not(:disabled)').on("change", function () {
        var configurator_block = jQuery(this).parents('.js-gift-configurator');
        gift_configurator(configurator_block);
    });

    // авторизация через соц сети
    $('#connectvk, #connectfb, #connectgoogle').click(function () {
        var data = jQuery(this).data();
        $.oauthpopup({
            path: data.url,
            callback: function () {
                $.get(data.redirect, function (data) {
                    window.location.replace('/auth/oauth/registration');
                });
            }
        });
    });

    //редирект формы онлайн оплаты
    jQuery("form.js-online-payment.js-redirect").submit();

    //баннер со сменяющейся картинкой
    if (jQuery('.js-change-image-banner > img').length > 0)
        setInterval(changeImg, 3000);

    function changeImg() {
        var visibleImg = jQuery('.js-change-image-banner > img:visible')
        if (visibleImg.next("img").length > 0) {
            visibleImg.hide().next('img').show();
        } else {
            visibleImg.hide().parent().children("img:first-child").show();
        }
        /*html
         <div class="js-change-image-banner">
         <img class="block" src="/images/c4ebb96cacef05.JPG"/>
         <img class="hide" src="/images/c4e58d7e6ac732.JPG"/>
         <img class="hide" src="/images/c4f8d6b0778b99-2.JPG"/>
         </div>
         */
        //
    }

    //сменяющиеся баннеры
    if (jQuery('.js-change-banner > a').length > 0)
        setInterval(changeA, 3000);

    function changeA() {
        var visibleA = jQuery('.js-change-banner > a:visible')
        if (visibleA.next("a").length > 0) {
            visibleA.hide().next('a').show();
        } else {
            visibleA.hide().parent().children("a:first-child").show();
        }
    }

    // заказ - дополнение к заказу
    jQuery(this).on('click', '.js-addtoorder-checkbox', function () {
        if (jQuery(this).is(':checked')) {
            jQuery('.js-cart-submit').addClass('el-button_prim-0').removeClass('cart-widget__but_disabled el-button_scnd-1');
        } else {
            jQuery('.js-cart-submit').removeClass('el-button_prim-0').addClass('cart-widget__but_disabled el-button_scnd-1');
        }
    });

    //кнопка отправки заказа
    jQuery(this).on('click', '.js-cart-submit', function () {
        if (parseFloat(jQuery('.js-cart-total-summ-block').attr('summ')) >= orderMinSumm || jQuery('.js-addtoorder-checkbox').is(':checked')) {
            jQuery('#ItemsOptions').submit();
        } else {
            var text = 'Минимальная сумма заказа 1500 рублей. Добавьте товаров или увеличьте их количество.';
            if (jQuery('.js-order-is-add-to-order-block').length > 0)
                text += ' Если заказ является дополнением к уже сделанному заказу, то отметьте соответствующий пункт.';
            flashMessenger('info', [text], 5000);
        }
        return false;
    });

    //отображение поп апа подписки - скидки
    if (jQuery('.lightbox-sendsay.display').length) {
        showSendsayPopup();
    } else if (jQuery('.lightbox-sendsay.can_display_by_timeout').length) {
        var timeoutID = window.setTimeout(showSendsayPopup, 60000);
    }

});

//функция загрузки/обновления виджета корзины
var ajaxLoadCartContent = null;
var firstOpenCart = false;
// function loadCartContent () {
// if (ajaxLoadCartContent != null)
// ajaxLoadCartContent.abort();
// var clicked = jQuery(".js-ajax-load-cart-content");
// ajaxLoadCartContent = jQuery.ajax({
// type: "POST",
// url: "/cart/index/index",
// dataType: "json",
// error: function (XMLHttpRequest, textStatus, errorThrown) {
// jQuery(".js-ajax-load-cart-content .cart-content-block").addClass('ajax-error').html('Идет загрузка... Пожалуйста, подождите.');/*'Во время AJAX запроса произошла ошибка: '+textStatus+'!'*/
// },
// success: function (data) {
// if (data.success == true) {
// if (data.cartTotalCount > 0) {
// jQuery(".js-ajax-load-cart-content").attr('sync', 'true');
// jQuery(".js-ajax-load-cart-content .cart-summ").html(data.cartTotalSumm);
// jQuery(".js-ajax-load-cart-content").addClass('header__but_cart-full');
// jQuery(".js-ajax-load-cart-content").removeClass('js-disabled');
// jQuery(".js-ajax-load-cart-content").attr('data-count', data.cartTotalCount);
// jQuery(".js-ajax-load-cart-content .cart-content-block").removeClass('ajax-error').html(data.html);
// } else {
// if (firstOpenCart) closeLightBox();
// jQuery(".js-ajax-load-cart-content").removeClass('header__but_cart-full');
// jQuery(".js-ajax-load-cart-content").addClass('js-disabled');
// jQuery(".js-ajax-load-cart-content .cart-summ").text('Корзина пуста');
// }
// pagination.init();

// //обновление данных о корзине в sendsay.ru
// if (typeof sndsyApi != 'undefined' && typeof data.sendsay != 'undefined') {
// if (data.sendsay.email != '') {
// sndsyApi.basketUpdate( data.sendsay.data, data.sendsay.email );
// } else {
// sndsyApi.basketUpdate( data.sendsay.data );
// }
// }
// }
// firstOpenCart = true;
// }
// });
// }


var hideMessage = '';

function flashMessenger(type, messages, timeout) {
    var template = '<div class="alert alert-dismissible"> \
      <div class="wrapper"> \
        <button aria-hidden="true" data-dismiss="alert" class="flash-message-block__del el-button el-del el-del_big" type="button"></button><ul></ul> \
        </div> \
      </div>';


    jQuery('.flash-message-block').not('js-flash-noanimate').slideDown('slow').append(template);
    var block = jQuery('.flash-message-block').children(':last-child');
    block.slideDown().addClass('alert-' + type);
    jQuery.each(messages, function (index, message) {
        block.find('ul').append('<li class="flash-message-block__item">' + message + '</li>');
    });

    if (timeout !== undefined) {
        console.log(jQuery('.flash-message-block').children().length);
        clearTimeout(hideMessage);
        hideMessage = setTimeout(function () {
            jQuery('.flash-message-block').slideUp();
            jQuery('.flash-message-block').children().slideUp();
        }, timeout);
    }
}

// $(document).on('keydown', '.price-min', function(event){
//     //event.preventDefault();
//     /* Act on the event */
//     var val = $(this).val();
//     var val2 = $(this).nextAll('.price-max').val();
//     console.log(val, val2);
//     if (val > val2){
//
//         return;
//
//     }
// });

// функции подбора картриджей
function updateTypesSelect() {
    if (userChoise.brandId != '') {
        var brandId = userChoise.brandId;
        jQuery.ajax({
            type: "POST",
            url: proxy,
            dataType: "json",
            data: {
                url: API_HOST + '/rashodnik/user/gettypes',
                id: brandId
            },
            success: function (data) {
                jQuery('.js-ajax-cartrijes-form #type').children('option:not(:first)').remove();
                options(".js-ajax-cartrijes-form #type", data);
            }
        });
    } else {
        jQuery('.js-ajax-cartrijes-form #type, .js-ajax-cartrijes-form #series, .js-ajax-cartrijes-form #model, .js-ajax-cartrijes-form #sendIt').attr('disabled', 'disabled');
        jQuery('.js-ajax-cartrijes-form #type').children('option:not(:first)').remove();
    }
}

function updateSeriesSelect() {
    if (userChoise.brandId != '') {
        jQuery.ajax({
            type: "POST",
            url: proxy,
            dataType: "json",
            data: {
                url: API_HOST + '/rashodnik/user/getseries',
                id: userChoise.brandId,
                idType: userChoise.typeId
            },
            success: function (data) {
                jQuery('.js-ajax-cartrijes-form #series').children('option:not(:first)').remove();
                options(".js-ajax-cartrijes-form #series", data, userChoise.seriesId);
            }
        });
    } else {
        jQuery('.js-ajax-cartrijes-form #series').children('option:not(:first)').remove();
    }
}

function updateModelsSelect() {
    if (userChoise.brandId != '') {
        jQuery.ajax({
            type: "POST",
            url: proxy,
            dataType: "json",
            data: {
                url: API_HOST + '/rashodnik/user/getmodels',
                idBrand: userChoise.brandId,
                idType: userChoise.typeId,
                idSeries: userChoise.seriesId
            },
            success: function (data) {
                jQuery('.js-ajax-cartrijes-form #model').removeAttr('disabled');
                jQuery('.js-ajax-cartrijes-form #model').children('option:not(:first)').remove();
                jQuery.each(data, function (keyObj, valObj) { //массив объектов из json с моделями принтеров для заданной марки
                    jQuery.each(valObj, function (keyObjModel, valObjModel) {
                        jQuery('<option value="' + valObj.id + '">' + (valObj.p_series !== undefined ? valObj.p_series + ' ' : '') + valObj.p_number + '</option>').appendTo('#model');
                        return false;
                    })
                })
            }
        });
    } else {
        jQuery('.js-ajax-cartrijes-form #model').children('option:not(:first)').remove();
    }
}

function updateSubmitButton() {
    if (userChoise.modelId != '')
        jQuery('#sendIt').removeAttr('disabled');
    else
        jQuery('#sendIt').attr('disabled', 'disabled');
}

function options(selector, objArray, select) {
    var sel = jQuery(selector);
    sel.removeAttr('disabled');
    jQuery.each(objArray, function (key, obj) { //массив объектов из json с моделями принтеров для заданной марки
        sel.append('<option ' + (select == obj.id ? 'selected="selected"' : '') + ' value="' + obj.id + '">' + obj.name + '</option>');
    })
}

function updatePrinterFullName() {
    var fullName = [];
    jQuery('.js-ajax-cartrijes-form #brand, .js-ajax-cartrijes-form #series, .js-ajax-cartrijes-form #model').each(function () {
        if (jQuery(this).val() != '')
            fullName.push(jQuery(this).children('option:selected').text());
    });
    jQuery('.js-ajax-cartrijes-form input#printerfullname').val(fullName.join(' '));

}
// конец функций подбора картриджей

// функция для подсчета подарков по акции
function gift_configurator(configurator_block) {
    var summ = 0;
    var credit = parseInt(configurator_block.find('.js-gift-credit').text());
    configurator_block.find('.js-gift-block').each(function () {
        var price = parseInt(jQuery(this).find('.js-gift-price').text());
        var count = jQuery(this).find('.js-input-spin').val();
        summ = summ + price * count;
    });
    summ = summ.toFixed(0);
    var difference = credit - summ;
    difference = difference.toFixed(0);
    configurator_block.find('.js-gift-summ').text(summ);
    configurator_block.find('.js-gift-difference').text(difference);
    configurator_block.find('.js-gift-block').each(function () {
        var price = parseInt(jQuery(this).find('.js-gift-price').text());
        var count = parseInt(jQuery(this).find('.js-input-spin').val());
        var addcount = Math.floor(difference / price);
        if (price > difference)
            jQuery(this).find('.js-input-spin').trigger("touchspin.updatesettings", {
                max: count
            });
        else
            jQuery(this).find('.js-input-spin').trigger("touchspin.updatesettings", {
                max: addcount + count
            });
    });
}

//функция показа поп апа сендсея
function showSendsayPopup() {
    jQuery('.lightbox__content').hide();
    jQuery('#lightbox-sendsay').show().parents('.lightbox,.lightbox__content').show();
}
