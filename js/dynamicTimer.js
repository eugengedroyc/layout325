class dynamicTimer {
	constructor(func, delay) {
		this.callbacks = [func];
		this.triggerTime = +new Date + delay;
		this.timer = 0;
		this.updateTimer();
	}

	updateTimer() {
		let that = this;
		clearTimeout(that.timer);

		let delay = that.triggerTime - new Date;

		this.timer = setTimeout(function () {
			for (let i = 0; i < that.callbacks.length; i++) {
				that.callbacks[i]();
			}

			that.resetTimer();
		}, delay);

		return this
	}

	setTime(delay) {
		this.triggerTime = +new Date + delay;
		this.updateTimer();
		return this
	}

	addTime(delay) {
		this.triggerTime += delay;
		this.updateTimer();
		return this
	}

	setFunc(func) {
		this.callbacks = [func];
		this.updateTimer();
		return this
	}

	addFunc(func) {
		this.callbacks.push(func);
		this.updateTimer();
		return this
	}

	add(func, delay) {
		this.triggerTime += delay;
		this.callbacks.push(func);
		this.updateTimer();
		return this
	}

	resetTimer() {
		clearTimeout(this.timer);
		this.callbacks = [];
		this.triggerTime = 0;
		this.timer = 0;

		return this
	}
}

// let timer = new dynamicTimer(function() {
// 	console.log("2+2 =", 2 + 2)
// }, 50) // Должно сработать почти мгновенно

// timer.addTime(1000) // Но мы продлим ещё на секунду