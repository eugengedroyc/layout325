export default class {
    constructor(outhNode) {
        this.activePopUp = null;
        this.node = outhNode;
    }
    closeHandler(event) {
        event.preventDefault();
        this.activePopUp.style.display = 'none';
        this.node.style.display = 'none';
        event.target.removeEventListener('click', this.closeHandler);
    }
    openHandler(event) {
        event.preventDefault();
        if(!event.target.matches('[data-id^=sign]')) {
            return;
        }
        const selector = event.target.dataset.id;
        this.activePopUp = this.node.querySelector(`[data-id=${selector}]`);

        const closeButton = this.activePopUp.querySelector('.outh_close');
        closeButton.addEventListener('click', this.closeHandler);
        this.node.style.display = 'block';
        this.activePopUp.style.display = 'block';

    }
    init() {
        this.openHandler = this.openHandler.bind(this);
        this.closeHandler = this.closeHandler.bind(this);
        document.addEventListener('click', this.openHandler);
    }

}
