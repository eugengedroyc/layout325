// Переменные
const tplPath = '.'; //template path

//для верхних закрепов
var headH = $(".header__main").outerHeight() + $(".header__top").outerHeight() + $(".js-header-height").outerHeight();
const headerFloat = $('#header-fixed');

var headerInfoTrg = ($('#js-header-info').length >= 1) ? $('#js-header-info').offset().top : 1000000;

var loaderTemplate = '<div id="js-loader"><img class="align-all" src="/local/templates/master_template/img/loader.gif" ></div>';
var pagination = {}, trgmenu = $('#baground_big_block');


function showFixHead() {
    let scrollDoc = $(window).scrollTop(), 
    topMenu = $('.header').height(),
    topProduct = ($('#js-header-info').length >= 1) ? $('#js-header-info').offset().top : 1000000;
    return [(scrollDoc >= topMenu), (scrollDoc >= topProduct)];
} 

function topLinePosition() {
    const [topMenu, topProduct] = showFixHead();

    if (topMenu) {
        $('#header-fixed').addClass('header-fixed_fixed');
        $('.header').addClass('sticky')//.height(topMenu);
        if ($('.header-info').length)
        $('.header-info').addClass('header-info_fixed');
    } else {
        $("#header-fixed").removeClass('header-fixed_fixed');
        $('.header').removeClass('sticky')//.height('auto'); 
        if ($('.header-info').length)
        $('.header-info').removeClass('header-info_fixed');
    };

    if (topProduct && !$('.lightbox:visible').length && !$('.el-lightbox-menu:visible').length ) {   
        $('.header-info').addClass('header-info_active');
        $("#header-fixed").addClass('header-fixed_header-info');
    } else {         
        $('.header-info').removeClass('header-info_active');
    }
}

export {showFixHead, topLinePosition};


//plugins
import "../10-bootstrap.min.js";
import "../jquery.bootstrap-touchspin.min.js"; 

// js анимация
import "../animate.js"; 

// меню
import "../nav-new.js"; 


/* ----------- Недостающий код ------------- */

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 *
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. "bold 14px verdana").
 *
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */

// функция подключения скриптов и стилей
function include(url) {
    if (url.match(/js$/g)) {
        var script = document.createElement('script');
        script.type = "text/javascript";
        script.src = url;
        document.body.appendChild(script);
    } else if (url.match(/css$/g)) {
        var style = document.createElement('link');
        style.rel = 'stylesheet';
        style.href = url;
        document.head.appendChild(style);
    }
}



function getTextWidth(text, font) {
    // re-use canvas object for better performance
    var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(text);
    return metrics.width;
}

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    alert('Код скопирован');
};

var clearPx = function (string) {
    return +string.replace('px', '')
}

function checkCheckbox() {
    if ($(this).prop('checked')) {
        $(this).parent().addClass('active');
    } else {
        $(this).parent().removeClass('active');
    }
    if ($(this).hasClass('form__checkbox-big')) {
        $(this).parent().addClass('form__checkbox_big');
    }
};

// добавление классов &_focus, &_empty к textarea с нестандарт. скроллом
var inputClass = 'el-textinput__input',
    emptyClass = inputClass+'_empty',
    focusClass = inputClass+'_focus';

$(document).on('keyup focus blur','.scroll-wrapper.'+inputClass+'_textarea textarea', function(){setTextLabels(this)});

function setTextLabels(textar) {
    var $textar = $(textar), textarVal = $textar.val(), parent = $textar.closest('.scroll-wrapper');
    !(textarVal.length>0)?parent.addClass(emptyClass):parent.removeClass(emptyClass); //empty
    $textar.is(':focus')?parent.addClass(focusClass):parent.removeClass(focusClass); //focus
}


window.openLightbox = function (el) {
    if (typeof el === 'string') {
        var trg = $(el);
    } else {
        var trg = el.data("target");
    }

    $('.lightbox__content').hide();
    $(trg).show().parents('.lightbox,.lightbox__content').show();
    $('.header-info').removeClass('header-info_active');

};

var closeLightBox = function () {
     if ($('#lightbox-sendsay').is(':visible')){
        $.post(
           "/app/feedback/disablesubscribeform"
         );
      };
      $('.el-lightbox-menu').fadeOut('fast');
      $('.js-lightbox-content').fadeOut('fast');
      $('.lightbox').fadeOut('fast').removeClass('active');//:not(.active) //@TODO клик по лайтбоксу
      $('.js-but').removeClass('active');
      topLinePosition()
};

var refreshTable = function () {
    $('.table').each(function(index, el) {
        var full = $(this).find('.table__tbody').children('.table__tr').length;
        if(full == 0){
            $(this).hide();
        }
        else{
            $(this).show();
        }
    });
};

var closeBubble = function (event){
    let $trg = jQuery(event.target);

    if ($trg.closest('.js-bubble:not(.item),.js-bubble_hover:not(.item),.bubble-conteiner').length){
        //console.log(1);
        return;
    } else{
        //console.log(2);
        $('.js-bubble').removeClass('active');
        $('.bubble-conteiner, el-bubble').fadeOut('100');
    }
};
$(document).on('click', '.js-bubble-close', function(e){
    e.preventDefault();  e.stopPropagation();
    $(this).closest('.bubble, el-bubble').hide();
});

var closeVariants = function (event){
    if (jQuery(event.target).closest('.js-this-openlist,.table-variants-conteiner').length){
        return;
    }
    else{
        $('.table-variants-conteiner').removeClass('active-table');
        $('.js-this-openlist').show();
    }
}; 

var closeOtherEl = function (event) {
    let $trg = $(event.target),
        $triggers = '.js-click-show__but,.js-month-picker',
        $targets = '.js-month-picker__target + *, .js-click-show__target';
    if ($trg.closest($triggers + $targets).length === 1){
        return
    } else {
        let otherEl = $($targets);
        for (let int = 0; otherEl.length > int; int++) {
            otherEl.eq(int).fadeOut();
        }
    }
};

function sliderRangeRefresh(){
    $('.slider-range').each(function(){
        /*==================== slider range  ====================*/
        var slider_range = {
            init : function(options){
                this.options = options || false;
                this._min = options._el_min || false;
                this._max = options._el_max || false;

                if( this.options ){
                    this.setSlider();
                    this.eventListeners();
                } else {
                    console.log('[ debug ]', 'Не были введены параметры'); return;
                }
            },

            eventListeners : function(){
                var options = this.options;
                var _this = this;
                $( this.options._el_min + "," + this.options._el_max ).on( "change", function() {
                    var price_min = +$( _this._min ).val().replace(/D/g, '');
                    var price_max = +$( _this._max ).val().replace(/D/g, '');
                    if ( price_min < options.min )
                        $( _this._min ).val( options.min );
                    else if( price_max > options.max )
                        $( _this._max ).val( options.max );
                    price_min = price_min >= options.min ? price_min : options.min;
                    price_max = price_max <= options.max ? price_max : options.max;
                    // if( price_min > price_max || price_max < price_min ){
                        if ( price_min > price_max ) {
                            $( _this._min ).val( --price_max );
                        } else if ( price_max < price_min ) {
                            $( _this._max ).val( ++price_min );
                        }
                    //     return;
                    // }
                    //  else{
                         $( options.el ).slider( "option", "values", [ price_min, price_max ] );
                // }
                });
            },

            setSlider : function(){

                var options = this.options;
                var _this = this;

                $( options.el ).slider({
                    range: true,
                    min: options.min,
                    max: options.max,
                    values: options.values,
                    step: options.step,
                    slide: function( event, ui ) {

                        $( _this._min ).val( ui.values[ 0 ] );
                        $( _this._max ).val( ui.values[ 1 ] );
                    }
                });
                $( _this._min ).val( options.values[0] );
                $( _this._max ).val( options.values[1] );
            }
        };

        var alias = $(this).data('alias');
        var min = $(this).data('min');
        var max = $(this).data('max');
        var step = $(this).data('step') != null ? $(this).data('step') : 1;
        // console.log(alias, min, max);
        // $( '#slider-range-'+alias ).slider({
        //   range:true,
        //   min: min,
        //   max: max,
        //   values: [min, max],
        //   slide: function( event, ui ) {
        //      $( "#price-min-"+alias ).val(ui.values[ 0 ] );
        //      $( "#price-max-"+alias ).val(ui.values[ 1 ] );
        //   }
        // });
        slider_range.init({
            el: '#slider-range-'+alias,
            _el_min: "#price-min-"+alias,
            _el_max: "#price-max-"+alias,
            min: min,
            max: max,
            values: [$("#price-min-"+alias).val(), $("#price-max-"+alias).val()],
            step: step
        });

    });
};

var cartScrollShow = '.js-scroll-show-full';

//сообщите об ошибке
document.onkeydown = ctrlEnter;
function ctrlEnter(event){
    if((event.ctrlKey) && ((event.keyCode == 0xA)||(event.keyCode == 0xD))) {
        jQuery('.js-paste-text').text(getSelectedText());
        $('.lightbox__content').hide();
        $('#lightbox-ctrlenter').show().parents('.lightbox,.lightbox__content').show();
    }
}

// функция для получение выделенного текста
function getSelectedText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection();
    } else if (document.getSelection) {
        text = document.getSelection();
    } else if (document.selection) {
        text = document.selection.createRange().text;
    }
    return text;
}



var labelTopRefresh = function(){
    setInterval(function(){
        $('.js-label-top').each(function(index, el) {
            var el = $(this);
            var inputs =  el.find('input,textarea,select');
            var text = inputs.val().length;
            if (inputs.val()){  
                 el.addClass('focus');
                 inputs.addClass('placeholder-no');
            }else{
                setTimeout(function(){
                    if (!inputs.hasClass('js-input-focus') && (text == 0 || inputs.val() == "") ){
                        el.removeClass('focus');
                        inputs.removeClass('placeholder-no');
                    }
                },100);
            }
        });
        //console.log($('.js-label-top').length);
    },100);
}

var paginationAnchor = function(el){
     var ajaxPag = el.parents('.js-ajax-pagination');
     var trg = el.parents('.js-catalog-items')
     if (ajaxPag.length >= 1){
         //console.log(ajaxPag.offset().top - 400);
         $('html, body').animate({scrollTop: trg.offset().top - 230},'fast');
     }
}

// clean js scroll function
// t - target DOM elem or px from top,
// s - step in px or miliseconds if intime == true,
// o - offset in px

/*  examples
    scrollToTarget(document.querySelector(trg), 50, offsetop); // step 50px
    scrollToTarget(document.querySelector(trg), 300, offsetop, true); // time 300ms
    scrollToTarget(300, 50); // offset from top 300px, step 50px
    scrollToTarget(300, 300, 0, true); // offset from top 300px, time 300ms
*/
window.scrollToTarget = function scrollToTarget (t, s, o, intime) {
    // o - offset in px
    var hh = o ? o : document.getElementById("header-fixed").getBoundingClientRect().height; // header height

    // s - step in px or miliseconds if intime == true,
    if (!s) {
        var step = false;
    } else if (intime) {
        var time = s, from, to;
    } else {
        var step = s;
    }

    // t - target DOM elem or px from top
    if (typeof t === 'number' || t instanceof HTMLElement) {
        var t = t;
    } else if (typeof t === 'string') {
        var t = document.querySelector(t);
    } else if (t instanceof jQuery) {
        var t = t[0];
    } else {
        var t = t[0];
    }

    function scrollAnim () {
        var ta, pos, top, newPos, to, from;

        function checkPos () {
            top = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0 // window top offset
            ta = (typeof t === 'number') ? (top - top * 2) + t : t.getBoundingClientRect().top; // target top offset
            pos = Math.round((typeof t === 'number') ? ta : ta - hh); // target top offset include header

            // if (top == 0) {
            //     return false
            // }

            //
            if (step != false && !time) {
                newPos = (pos > 0) ? top + step : top - step; // if pos is negative top - step (scroll to top)
            } else if (time) {
                from = top;
                to   = top + pos;
            } else {
                newPos = top + pos;
            }
        }

        function anim () {
            if (time) {
                animate({
                    duration: time,
                    timing: animateDefault,
                    draw: function(progress) {
                        var result = (to - from) * progress + from;
                        window.scroll(0,Math.ceil(result));
                    },
                    end: function() {
                        if (checkPosСonformity() !== 'equals') {
                            checkPos();
                            anim();
                        }
                    }
                });
            } else {
                window.scroll(0,newPos); // set new scroll position
            }
        }

        function checkPosСonformity () { // соответсвие нужной позиции от элемента
            checkPos();

            if (pos > 0) {
                if ((pos - step) > 0) {
                    return true
                }
            } else if (pos < 0) {
                if ((pos + step) < 0) {
                    return true;
                }
            } else if (pos == 0) {
                return 'equals'
            } else {
                return false;
            }
        }

        checkPos();
        anim();

        if (checkPosСonformity() === true) {
            requestAnimationFrame(scrollAnim);
        } else {
            if (!intime) {
                window.scroll(0, top + pos);

                requestAnimationFrame(function () {
                    if (checkPosСonformity() === 'equals') {
                        // console.log('equals');
                        return false;
                    } else {
                        step = false;
                        requestAnimationFrame(scrollAnim);
                    }
                });
            }
        };

        return false;
    }

    scrollAnim();
}


$( document ).ajaxComplete(function() {
  if($('.cart-widget-good').length < 2) {
      $(cartScrollShow).addClass('hidden')
  }
});

var isHidden = $elem => $elem.data('hidden');

var jsOpenCloseEngine = function ($el) {
    var trg = $el.data('trg'),
        $trg = $(trg),
        $but = $el.find('.el-arrow-but'),
        $butTxt = $el.find('.el-arrow-bubble__txt');

    var duration = {
        show : 300,
        hide : 100
    }

    var cookiesName = 'jsOpenClose ' + trg;

    var getHiderText = function () {
        if (isHidden($el)) {
            return $el.data('open');
        } else {
            return $el.data('close');
        }
    }

    var updateCookies = function () {
        if (isHidden($el)) {
            $.cookie(cookiesName, 'hidden')
        } else {
            $.cookie(cookiesName, null)
        }
    }

    var updateArrowButton = function () {
        if ($but) {
            var butTxt = getHiderText();

            if (isHidden($el)) {
                $but.addClass('el-arrow-but_bot').css('transition','transform ' + duration.hide + 'ms');
            } else {
                $but.removeClass('el-arrow-but_bot').css('transition','transform ' + duration.show + 'ms');
            }

            if ($butTxt && butTxt) {
                $butTxt.text(butTxt);
            }
        }
    }

    var updateToggle = function () {
        if (isHidden($el)) {
            $el.removeClass('active');
        } else {
            $el.addClass('active');
        }
    }

    var updateTarget = function (state) {
        if (isHidden($el)) {
            $trg.fadeOut(state === 'instant' ? 0 : duration.hide);
        } else {
            $trg.fadeIn(state === 'instant' ? 0 : duration.show);
        }
    }

    var updateTitles = function () {
        let notClass = 'el-arrow-wrapper';
        let titleTxt = getHiderText();

        $el.children().each(function () {
            let $that = $(this);

            if (!$that.hasClass(notClass)) {
                $that.attr('title', titleTxt);
            }
        });
    }

    var update = function (state) {
        updateTarget(state);
        updateToggle();
        updateArrowButton();
        updateTitles();
    }

    var setHidden = function (stat = true) {
        if (stat === false) {
            $el.data('hidden', false);
        } else {
            $el.data('hidden', true);
        }

        updateCookies();
    }

    var toggle = function () {
        if (isHidden($el)) {
            setHidden(false)
        } else {
            setHidden()
        }

        update();
    }

    var checkCookies = function () {
        if ($.cookie(cookiesName) === 'hidden') {
            setHidden()
        } else {
            setHidden(false)
        }

        update('instant');
    }

    checkCookies();
    update();

    $el.on('click', '> *', toggle);
}

var jsOpenClose = function () {
    var $el = $('.js-open-close');

    $el.each(function () {
        jsOpenCloseEngine($(this));
    });
}

$(function(){
    // Управление плавающими баннерами 
    $('.js-SideEvent-open').click(function () {
        $(this).closest('.SideEvent').addClass('SideEvent--open');
    });

    $('.js-SideEvent-close').click(function (e) {
        e.preventDefault();
        $(this).closest('.SideEvent').removeClass('SideEvent--open');
        return false;
    });

    // $(window).resize(debounce(function () {
    //     $('.SideEvent').removeClass('SideEvent--open');
    // }, 1000));
    // ~ Управление плавающими баннерами

    jsOpenClose();
    
    $(document).on('click', '.js-close', function(event){
        var trg = $(this).data('target');
        var session =  $(this).data('session');
        $(trg).hide();
        if(session){
            sessionStorage.setItem(session, false);
        }
    });

    $('#banner-capability').each(function(){ //баннер в ЛК
        var sessionVar = (sessionStorage.getItem('showBannerСapability') !== null) ? sessionStorage.getItem('showBannerСapability') : true;
        if (sessionVar === true){
          $(this).show();
        }
    });


    $('.js-month-picker__target').MonthPicker({ //Ахив
        i18n: {
           year: 'год',
           backTo: "назад к",
           months: [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ]
           // monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ]
        },
        'OnAfterMenuOpen': function() {
            $('.month-picker').find('.ui-button').removeClass('ui-state-disabled');//fix не переключаются года при открытие
        }
    });

    $(document).on('click', '.js-month-picker', function(event){
        $('.js-month-picker__target + *').click();
    });

    $(document).on('click', '.js-click-show__but', function(event){
         $(this).next('.js-click-show__target').toggle();
    });

    $('.form__select select').each(function(){$(this).selectmenu()});
    $(document).on('click', '.js-loadPhoto', function(event){
        $('#loadPhoto').click();
    });

    $(document).on('change', '.personal_data__form input[type="radio"]', function(event){
        let form = $(this).parents('.personal_data__form'),
            select = form.find( '.select-ico' ),
            text = form.find( 'input[type="text"]' );
        if ($('#company2').is(':checked')){
            //form.find( 'select' ).selectmenu( 'disable' );
            select.addClass('select-ico_disabled');
            text.attr( 'disabled','disabled' );
        } else {
            //form.find( 'select' ).selectmenu( 'enable' );
            select.removeClass('select-ico_disabled');
            text.removeAttr( 'disabled' );
        }
    });

    /** Оформление заказа **/
    $(document).on('click', cartScrollShow, function(event){
        console.log($(this));
        $(this).toggleClass('opened');
        $('.js-scroll-cartitem').toggleClass('opened');
        if ($(this).hasClass('opened')){
            setTimeout(function(){
                $('.js-scroll-cartitem').addClass('scrolled');
            },500);
        } else {
            $('.js-scroll-cartitem').removeClass('scrolled');
        }
    });
    /** /Оформление заказа **/

    function emptynav () {
      if (!$('.nav.nav_categories').children().length > 0){
        var emptynav = jQuery.trim($('.nav.nav_categories').text());
        $('.nav.nav_categories').text(emptynav);
      }
    };
    emptynav();

    $('.toggle-wrap__item.toggle').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('.categories-wrap').toggleClass('closed');
    });

    $('#header-fixed').appendTo('#header-append-container').show();


    function jsCountMore(el) {
        var count = el.val().length;
        if (count > 3) el.addClass('js-count-more_1000');
        else el.removeClass('js-count-more_1000');
        if (count > 4) el.addClass('js-count-more_10000');
        else el.removeClass('js-count-more_10000');
        if (count > 5) el.addClass('js-count-more_100000');
        else el.removeClass('js-count-more_100000');
    };

    var duplicateText = function(el){ //подстановка числа (в корзине из инпута в цену)*
        var txt = parseFloat(el.find('.js-duplicate__source').val());
        //console.log(txt != 1);
        el.find('.js-duplicate__trg').text(txt); //*только класс подставляется
        if (txt != 1){
            el.find('.js-duplicate__trg').parent().addClass('active');
        }else{
             el.find('.js-duplicate__trg').parent().removeClass('active');
        }
    };  duplicateText($('.js-duplicate__source'));


    $('.js-count-more, .js-input-spin').on('keyup change', function(event){
        let valSource = $(this).val(),
            valComplete = (valSource > 0) ? valSource : 1;
        $(this).val(valComplete);

        if($(this).hasClass('js-duplicate__source')){
            duplicateText($(this).parents('.js-duplicate'));
        }
        jsCountMore($(this));
    }).each(function(index, el) {
       
        if($(this).hasClass('js-duplicate__source')){
            duplicateText($(this).parents('.js-duplicate'));
            //console.log(el)
        }
        jsCountMore($(this));
    });

    if (document.location.href.split('/')[3] == 'page'){//страницы статей
        $('body').addClass('body-grey');
        switch (document.location.pathname){
            case '/page/howtoorder':
            case '/page/deliver':
            case '/page/delivery':
            case '/page/payment':
            case '/page/assembly':
            case '/page/rigging':
            case '/page/about':
            case '/page/contact':
            case '/page/conditions':
            case '/page/opentime':
            case '/page/helper':
            case '/page/polzovatelskoe-soglashenie':
                $('body').removeClass('body-grey');
                break;
            default:
        };
    }

    //остальные страницы
    if (document.location.pathname == '/pages/reviews'){
        $('body').addClass('body-no-grey');
    }
    if (document.location.href.split('/')[5] == 'login'){
        $('body').addClass('body-grey');
        if (document.location.pathname == "/auth/user/login/first/true"){
            $('body').removeClass('body-grey').addClass('body-empty');
        }
    }


    var clJsShow = {
        self : '.js-show-only',
        get wrap() {
            return this.self + '__wrap'
        },
        get but() {
            return this.self + '__but'
        },
        get butActive() {
            return this.but + '_active'
        },
        get item() {
            return this.self + '__item'
        }
    };

    function jsShowOnly(parent,itemsToShow) { // hide
        var $item = $(parent).find(clJsShow.item),
            $btn = $(parent).find(clJsShow.but);

        $item.slice(itemsToShow).hide();
        $item.slice(0, itemsToShow).show();
        $btn.removeClass(clJsShow.butActive.replace('.',''));
    }

    function jsShowAll(parent) { // show
        var item = $(parent).find(clJsShow.item),
            $btn = $(parent).find(clJsShow.but);

        item.slideDown(100);
        $btn.addClass(clJsShow.butActive.replace('.',''));
    }

    $(clJsShow.self).each(function(){
        var show = $(this).data('show');
        jsShowOnly(this, show);
    });

    $(document).on('click', clJsShow.but, function(event) {
        event.preventDefault();
        var but =  $(this);

        var parent = but.parents(clJsShow.self),
            cont = parent.find(clJsShow.wrap),
            show = parent.data('show');

        var event = but.data('event'),
            txtClose = but.data('close'),
            txtOpen = but.data('open');

        if (event == 'hide') {
            cont.slideUp(300);
            jsShowOnly(parent,show);
            but.data('event','show').text(txtOpen);
        } else if(event == 'show') {
            cont.slideDown(300);
            jsShowAll(parent);
            but.data('event','hide').text(txtClose);
        }
    });

    $('.js-sync-input.js-sync-input_variant').change(function(event) { //синхронизация количества товара
        $('.js-sync-input.js-sync-input_variant').val($(this).val());
    });

    $('#go-to-top').click(function() {
        scrollToTarget(0, 300, 0, true);
    });

    $('.js-notice-close').on('click', function (){ //скрытие при клике по крестику
        var alias = $(this).data('alias');
        var date = $(this).data('date');
        // console.log(alias,date);
        $.post(
           "/app/feedback/disablenotice",
           {
               alias: alias,
               date: date,
           }
        );
    });

    $('.flash-message-block').bind('close.bs.alert', function (){ //скрытие при клике по крестику
        $(this).slideUp();
    });

    //костыль по лэйблам
    // jQuery('.form__item').addClass('form__item_labeltop');
    jQuery('#Review').find('.form__item').removeClass('form__item_labeltop js-label-top');//обычная форма: отзывы

    //лэйбл наверху
    $('.js-label-top').on('focus', '.form__input, .el-textinput', function(e) {
        $(this).parent().addClass('focus');
        var inputs =  $(this).find('input,textarea,select');
        inputs.addClass('placeholder-no js-input-focus');
        //$('[name="phone"],.js-phone').mask();
    }).on('blur', '.form__input, .el-textinput', function(e) {
        var el = $(this);
        var inputs = el.find('input,textarea,select');
        var text = inputs.val().length;
        setTimeout(function(){
            if (text == 0 || inputs.val() == ""){
                el.parent().removeClass('focus');
                inputs.removeClass('placeholder-no  js-input-focus');
            }
        },100);

        //console.log(text,inputs.val());
        //$('[name="phone"],.js-phone').unmask();
    }).on('change', '.form__input, .el-textinput', function(event) {
        event.preventDefault();
    });;

    var options =  {
      onComplete: function(cep) {
        alert('CEP Completed!:' + cep);
      },
      onInvalid: function(val, e, f, invalid, options){
        var error = invalid[0];
        console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);
      }
    };
    //маска для телефона +7 (___) ___-__-__
    $('[name="phone"],.js-phone').mask('+7 (999) 999-99-99', {autoclear: false});

    //переход по всем ссылкам по бланку
    $('.js-target-blank').on('click', 'a:not(.js-ajax-addtocart)', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        window.open(href);
    });

    //подбор замены товара
    $(document).on('click', '.js-good-replace', function(event) {
        var prop = $(this).data('prop');
        $('.good-replace__properties').children().hide();
        $('.good-replace__properties').children(prop).show();
    });

    //страница картриджей
    $(document).on('click', '.js-addclass-cartridge', function(event) {
        $('.js-addclass-cartridge').addClass('cartridge-choose_deactive');
        $(this).removeClass('cartridge-choose_deactive');
    });
    //добавление класса
    $(document).on('change', '.js-addclass-check', function(event) {
        $('.js-addclass-check').each(function(index, el) {
            var trg = $(this).data('trg');
            var elclass =  $(this).data('class');
            if(this.checked) {
                $(trg).addClass(elclass);
            }
            else{
                 $(trg).removeClass(elclass);
            }
        });
    });

    //удаление класса
    $(document).on('change', '.js-removeclass-check', function(event) {
        $('.js-removeclass-check').each(function(index, el) {
            var trg = $(this).data('trg');
            var elclass =  $(this).data('class');
            if(this.checked) {
                $(trg).removeClass(elclass);
            }
            else{
                 $(trg).addClass(elclass);
            }
        });
    });

    //рейтинг по звездочкам
    $(document).on('click', '.js-rating>span', function(event) {
        event.preventDefault();
        /* Act on the event */
        var grade = $(this).data('grade');
        $('.js-rating>span').removeClass('active');
        $(this).addClass('active');
        $('[name="grade"]').val(grade);
        //console.log($('[name="grade"]').val());
    });
    // все бренды
    $('.js-alphabet-item').on('click', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $('.js-alphabet-target').removeClass('active');
        $('#'+id).addClass('active');
        $('.js-alphabet-item').removeClass('active');
        $(this).addClass('active');
    });

    $('#banners-grid').masonry({
      // options
      itemSelector: '.js-banners-grid',
      columnWidth: 309,
      gutter: 15
    // percentPosition: true
    });

    // изо-ия в карточке товара - модальное окно
    // var $= jQuery.noConflict();
    // jQuery(".fancybox-thumb").fancybox({
    //     prevEffect: 'fade',
    //     nextEffect: 'fade',
    //     maxWidth: 730,
    //     maxHeight: 730,
    //     helpers: {
    //         title: {
    //             type: 'inside'
    //         },
    //         thumbs: {
    //             width: 80,
    //             height: 80,
    //             position: 'bottom'
    //         }
    //     }
    // });


    // jQuery("#js-image-block").on('click', '.js-fancybox-thumb', function(event) {
    //     event.preventDefault();
    //     /* Act on the event */
    //     var href = $(this).attr('href');
    // });

    var jsVideoLightbox = jQuery('#js-video-lightbox').length;
    var jsScrollImages = jQuery('.js-scroll_additional_images').length;
    if (jsScrollImages == 0) {
        jQuery('#js-video-lightbox').addClass('only');
    }else{
        //инициализация дополнительных изображений
        jQuery('.js-scroll_additional_images').each(function(index, el) {
        var num = $(this).find('.js-scroll_additional_items').length;
        var h = $(this).find('.js-scroll_additional_items').eq(0).outerWidth(true);

        if (num < 5 && jsVideoLightbox){
            $(this).height(num*h);
        }
        else if (num > 4 && jsVideoLightbox){
            //console.log(num.length, jQuery('.js-scroll-additional_images-button').length);
            jQuery('.js-scroll-additional_images-button').addClass('active');
            $('.js-carousel-variant').addClass('carousel-variant_video');
        }else if (num > 5){
            jQuery('.js-scroll-additional_images-button').addClass('active');
        }else{
            $(this).parent().addClass('deactive');
        }
        });
    }
    jQuery('.js-video-lightbox').each(function(index, el) {
        var src = $('#video').find('iframe').attr('src');
        if (src){
        $(this).attr('href', src);
        }
        //console.log(src);
    });

    //прокрутка дополнительных изображений вверх вниз по стрелочками
    jQuery('.js-scroll-additional_images-button').click(function () {
    if (!$(this).hasClass('deactive')){
        var scrollStep = jQuery('.js-scroll-additional_images-block a').eq(0).outerWidth(true);
        var move = jQuery(this).attr('move');
        var curTopPos = parseInt(jQuery('.js-scroll-additional_images-block').css('left'));
        //console.log(scrollStep,move,curTopPos,jQuery('.js-scroll-additional_images-block').parent().height() - jQuery('.js-scroll-additional_images-block').height());
        if (move == 'up' && curTopPos >= 0 ||
            move == 'down' && curTopPos <= (jQuery('.js-scroll-additional_images-block').parent().width() - jQuery('.js-scroll-additional_images-block').width())+4
        ){
        $(this).addClass('deactive');
        return false;
        }
        else{
        $('.js-scroll-additional_images-button').removeClass('deactive');
        var sign = (move == 'up') ? '+' : (move == 'down') ? '-' : '';
        jQuery('.js-scroll-additional_images-block').animate(
            {'left': sign + '=' + scrollStep + 'px'}, 100);
        }
    }
        console.log(curTopPos);
    });



    //якоря
    //при загрузке
    var anchorTrg = document.location.hash;
    if (anchorTrg != "") {
        // if ()
        $('html, body').animate({scrollTop:$(anchorTrg).offset().top - 50},'fast');
    }

    $(document).on('click', '.js-anchor', function(event){
        event.preventDefault();
        var trg = $(this).attr('href');
        var click = $(this).data('click');
        var offsetop = $(this).data('top') ? $(this).data('top') : 160; //@TODO сделать просчет всплывашек по высоте
        if (click) {
            $(click).click();
        }
        // $('html, body').animate({scrollTop:$(trg).offset().top - offsetop},'fast');
        scrollToTarget(document.querySelector(trg), 300, offsetop, true);

        $(this).mouseleave();
    });

    if ($('.mod-cart.con-index.act-index').find('.bubble_error').length >= 1){
        $('.mod-cart.con-index.act-index').each(function(index, el) {
            var anchorTrg = $(this).find('.bubble_error').eq(0);
            $('html, body').animate({scrollTop: anchorTrg.offset().top - 115}, 'slow');
        });
    }

    $(document).on('click', 'a[href="#"]', function(event) {
        event.preventDefault();
        //   return false;
    });

    //переход по всем ссылкам по бланку
    $(document).on('click', '.js-link', function(e) {
        var href = $(this).data('href');
        document.location.href = href;
    });

    $('.js-input-error input').change(function(event) {
        $(this).parents('.js-input-error').find('.js-input-error__bubble').hide();
    });

    // выделение по всей строке
    $(document).on('click','.js-click-parent', function(event){
        let href = '.js-href-children',
            check = '.js-click-children',
            all = '.el-button, .table__td_button';
        if ($(event.target).closest(href+','+check+','+all).length){
            return;
        } else {
            var checkbox = $(this).find(check);
            var link = $(this).find(href).attr('href');
            checkbox.prop('checked',!checkbox.prop("checked"));
            window.location.href = link;
        }
    });

    //FORM
    // file
    $(document).on('click','.form__file,.form__file-trg', function(){
        var file = $(this).siblings('input');
        //console.log(file);
        file.click();
        $(file).change(function(event) {
        var name = file.val();
        //console.log(name);
        if(name.indexOf('C:\\fakepath\\')+1){
            name = name.substr(12);
        }
        $('.form__file-trg input').val(name);
        });
    });

    //страница помощь по сайту
    $('#b-help').on('click', '.js-load-slider', function(event) {
        event.preventDefault();
        /* Act on the event */
        var trg = $(this).attr('href');
        $('.js-load-slider-this').hide();
        $(trg).show();
        $('.js-load-slider').removeClass('active');
        $(this).addClass('active');
        $('.js-slider_help').slick('setPosition');//$('.js-load-slider-this').show().hide()
    });
    $('.js-load-slider_first').click();

    // checkbox
    $(document).on('change','.el-checkbox input,.form__checkbox>input', checkCheckbox);

    //равная высота меню
    $('#nav_list').each(function(){
    var h = $(this).outerHeight();
    $(this).find('.nav_list').css('min-height',h);
    })


    //------------------- Кнопки с лайтбоксом------------------------------------
    $('#close-lbox').on("click", function() {
      closeLightBox();
    });
    //события по клику в любом месте страницы кроме нужных элементов
    jQuery(document).on('click', function(event){
         closeBubble(event); 
         closeVariants(event); 
         closeOtherEl(event);
    }).on('mousedown', '.lightbox', function(event){
        if (jQuery(event.target).closest('.lightbox__content').length){
            return;
        }
        else{
            closeLightBox();
        }
    }).on('click', '.header', function(event){
        if (jQuery(event.target).closest('.js-but,.js-menuopen').length){
            return;
        } else {
            // closeLightBox();
        }
    });

    $('.js-but').on("click", function(event) {
        if(!$(this).hasClass('js-disabled')){
            if(!$(this).hasClass('active')){
              var trg = $(this).data('target');
              closeLightBox();
              $(trg +',#lightbox').fadeIn('fast');
              $(this).addClass('active');
              $('.header-info').removeClass('header-info_active');
            }
            if (jQuery(event.target).closest('#but_cart').length){//.el-button,
                if (jQuery(event.target).closest('.js-cart-info__next,.js-cart-info__prev').length){
                   return
               }
               else{
                //    pagination.init();
               }
            }
        }
    });


    $(document).on('click','.js-nolink',function(event){
        event.preventDefault();
    }).on('click','.js-nolink a',function(event){
        event.stopPropagation();
    });

    $('.js-but_hover').hoverIntent({
        over: function(event) {
            if(!$(this).hasClass('js-disabled')){
                if(!$(this).hasClass('active')){
                    var trg = $(this).data('target');
                    $(trg).fadeIn('fast');
                    $(this).addClass('active');
                    $('.header-info').removeClass('header-info_active');
                    $(document).trigger('close_main_menu');
                }
            }
        },
        // timeout: 350,
        out: function(event) {
            var trg = $(this).data('target');
            $(trg).fadeOut('fast', function(){
                topLinePosition();
            });
            $(this).removeClass('active');
        }
    });

    //lightbox
    $(document).on('click','.js-lightbox', function (){ openLightbox($(this))});

    $('.js-lightbox_close').on("click", function(){
        // $(this).parents('.lightbox').hide();
        closeLightBox();
    });

    /* раскрывающийся список */
    // с ссылкой
    $('.js-list-unwrap').on('click','.js-list-unwrap__item', function(event) {
        event.preventDefault();
        // пофиксить этот код
        // [ jQuery(this).parent().hasClass('active') ? 'removeClass' : 'addClass' ]('active');
        if ($(this).parent().hasClass('active')){
            $('.js-list-unwrap').removeClass('active');
            $('.js-list-unwrap__target').slideUp();
            $(this).parent().removeClass('active');
            $(this).siblings('.js-list-unwrap__target').slideUp();
        }else{
            $('.js-list-unwrap').removeClass('active');
            $('.js-list-unwrap__target').slideUp();
            $(this).parent().addClass('active');
            $(this).siblings('.js-list-unwrap__target').slideDown();
        }
    }).each(function(){
        if ($(this).find('a').hasClass('active')){
            $(this).addClass('active');
            //console.log($(this).find('.js-list-unwrap__target').length,'sdfsd');
            $(this).find('.js-list-unwrap__target').show();
        }
    });

    // без ссылки
    $('.js-list-unwrap2').on('click','.js-list-unwrap2__item', function(event) {
        $('.js-list-unwrap2__item').removeClass('active');
        $(this).addClass('active');
    });

    // хар-ки динамическая ширина
    $('.js-dynamic-width__tr').each(function(index, el) {
        var width = $('.js-dynamic-width').width();
        var elWidth1 = $(this).find('.js-dynamic-width__td:first-child');
        var elWidth2 = $(this).find('.js-dynamic-width__td:last-child');
        var elWidth1text = elWidth1.find('.js-dynamic-width__content').width();
        var elWidth2text = elWidth2.find('.js-dynamic-width__content').width();
        if(elWidth1text > width/2 ){
            elWidth1.width(elWidth1text);
            elWidth2.width(width-elWidth1text);
        }else if(elWidth2text > width/2){
            elWidth2.width(elWidth2text);
            elWidth1.width(width-elWidth2text);
        }
    });

    //bubble
    var timeBubble = '';
    $(document).on('click','.item, .js-bubble_hover', function(event) {
        var trg = ($(this).attr('data-bubble') !== undefined) ? $(this).find($(this).data('bubble')) : $(this).next('.bubble-conteiner');
        trg.fadeOut(100);
        console.log(trg);
    });
    $(document).on('click','.js-bubble', function(event) {
        var trg = ($(this).attr('data-bubble') !== undefined) ? $(this).find($(this).data('bubble')) : $(this).next('.bubble-conteiner');
        $('.js-bubble').not(this).removeClass('active').find('.bubble-conteiner').fadeOut(100);
        $('.js-bubble').not(this).next('.bubble-conteiner').fadeOut(100);
        $(this).addClass('active');
        if (trg.length){
            trg.fadeIn(100);
        }
        else{
            var trg = $(this).find('.bubble-conteiner');
            trg.fadeIn(100);
        }
    });

    $(document).hoverIntent({
        over: function(){
            var trg = ($(this).attr('data-bubble') !== undefined) ? $(this).find($(this).data('bubble')) : $(this).next('.bubble-conteiner');
            $('.js-bubble').removeClass('active');
            $('.bubble-conteiner').fadeOut(100);
            $('.js-bubble_hover').removeClass('active');
            $(this).addClass('active');
            if (trg.length || $(this).attr('data-bubble') !== undefined){
                trg.fadeIn(100);
            }
            else{
                var trg = $(this).find('.bubble-conteiner');
                trg.fadeIn(100);
            }
        },
        timeout: 200,
        out: function(){
            var trg = ($(this).attr('data-bubble') !== undefined) ? $(this).find($(this).data('bubble')) : $(this).next('.bubble-conteiner');
            //if $($(this).parents('#goods-list').)
            $('.js-bubble').removeClass('active');
            $('.js-bubble_hover').removeClass('active');
            //console.log(trg);
            if (trg.length){
                trg.fadeOut(100);
            } else {
                var trg = $(this).find('.bubble-conteiner');
                trg.fadeOut(100);
            }
        },
        selector: '.js-bubble_hover'
    });

    $(document).hoverIntent({
        over: function(){
            var $self = $(this);
                $('.item').data('hover', false);
                $self.data('hover', true);

            var $bbl = $self.find($self.data('bubble'));
                $bbl.fadeIn(100);

            $('.item').each(() => {
                var $self = $(this);

                // console.log($('.item').data('hover'));
                if ($self.data('hover') !== true) {
                    $self.find('.bubble-conteiner').fadeOut(100);
                }
            })

            var offset = $self.find('.js-item-anim-fade__dinam').outerHeight();//.find('.js-item-anim-fade).data('dinam')

            $self.find('.js-item-anim-fade').animate({top: -offset}, 200);
            $self.find('.js-item-anim-fade, .js-item-anim-fade__dinam').addClass('active');
        }, timeout: 350,
        out: function(){
            var $selfOut = $(this);
                $selfOut.data('hover',false);

                //   console.log($selfOut.data('hover'));
            $selfOut.find('.js-bubble').removeClass('active');
            $selfOut.find('.bubble-conteiner').fadeOut(100);
            $selfOut.find('.js-item-anim-fade, .js-item-anim-fade__dinam').removeClass('active');
            $selfOut.find('.js-item-anim-fade').animate({top: 0}, 150);
        },
       selector: '.item'
    });

    //замена элементов один на другой
    $(document).on('click','.js-switch', function(event) {
        var trg = $(this).data('target');
        // console.log(trg);
        $(this).fadeOut('fast', function() {
            $(trg).fadeIn('fast');
        });
    });

    //таблица покупки в листинге
    $(document).on('click','.js-this-openlist',function(){
        $('.table-variants-conteiner').removeClass('active-table');
        $('.js-this-openlist').show();
        $(this).hide();
        $(this).parents('.table-variants-conteiner').addClass('active-table');
    }).on('click','.js-hide-variants',function(){
        $('.table-variants-conteiner').removeClass('active-table');
        $('.js-this-openlist').show();
    });

    initSelects();

    // обработка el-arrow-bubble(подсказки) при однократном ховере на el-arrow-wrapper
    $(document).one('mouseenter', '.el-arrow-wrapper', () => {
        let $that = $(this);
        let $bubbleTxt = $that.find('.el-arrow-bubble__txt');

        // достаем самый длинный текст между data-close и data-open
        let getLongestData = function () {
            let close = $that.find('[data-close]').data('close'),
                open = $that.find('[data-open]').data('open'),
                longest;

            if (close.length > open.length)
                longest = close;
            else
                longest = open;

            return longest
        }

        // суммируем ширину текста с доп стилями по бокам
        let getOuterWidth = function () {
            let paddings = clearPx($bubbleTxt.css('padding-left')) + clearPx($bubbleTxt.css('padding-right')),
                text = getLongestData(),
                textFont = $bubbleTxt.css('font'),
                textWidth = Math.ceil(getTextWidth(text, textFont));

                return textWidth + paddings
        }

        $bubbleTxt.css('width', getOuterWidth()); // устанавливаем полученую ширину
    })
});

function checkAttr(attr) {
    if(typeof attr !== typeof undefined && attr !== false) { return true; } else { return false; }
}
//что бы ничего не полетело, классы менять только тут
var sIco = 'select-ico', // класс нового селекта
    sIcoJs = 'js-'+sIco,
    sIcoActive = sIco+'_active',
    sIcoLabel = sIco+'__label', // верхний лэйбл
    sIcoList = sIco+'__list', // выезжающий список
    sIcoListItem = sIco+'__list-item', //пункт списка
    sIcoListText = sIco+'__list-text', //текст пункта либо тег а либо span
    sIcoHasLabel = sIco+'_hasLabel', //класс наличия лэйбла, если нет то margin-top: 0
    sIcoHasFixedPlaceholder = sIco+'_hasFixedPlaceholder', //
    sIcoTitle = sIco+'__title', // обертка для плэйсхолдера и выбранного пункта
    sIcoTitleL = sIco+'__title__light', // плэйсхолдер
    sIcoTitleJs = sIco+'__title_js', // выбранный пункт
    sIcoHasVal = sIco+'_hasVal';//класс для css что бы показывал лэйбл


// - Функция setSelectItem устанавливает value у select и его
// прототипа .js-select-ico по этому лучше устанавливать value через неё.
// - id нужно передавать без #
// - в stealth передаем true если не нужно вызывать событие .change()
// к примеру stealth нужен в случае фильтров, так как на onchange
// перезагружается стр.
// - что бы не зафиксировать плэйсхолдер на селект нужно поставить атрибут data-placeholder-fixed
window.setSelectItem = (id,itemVal,stealth) => { // устанавливает активным пункт селекта
    var select   = $('#'+id), // тег select
        customS  = $('[data-id="'+id+'"]'), // прототип
        sTxt     = customS.find('[data-value="'+itemVal+'"] > .'+sIcoListText).html(), // название устанавливаемого value
        sTitle   = customS.find('.'+sIcoTitleJs), // выбранный пункт
        sTitleL  = customS.find('.'+sIcoTitleL); // плэйсхолдер
    select.children('[selected]').removeAttr('selected');
    select.children('[value="'+itemVal+'"]').prop('selected', true); // устанавливаем выбранный пункт на скрытом селектe
    if (!stealth) select.change(); // если не указан stealth режим, то вызываем событие .change()
    if (!checkAttr(select.data('placeholder-fixed'))) { // если не стоит атрибут data-placeholder-fixed, то очищаем содержимое плэйсхолдера
        sTitleL.html('');
    }
    (sTitle.length > 0)?sTitle.html(sTxt):(customS.hasClass(sIcoHasFixedPlaceholder))?sTitleL.after('<span class="'+sIcoTitleJs+'">'+sTxt+'</span>'):sTitleL.before('<span class="'+sIcoTitleJs+'">'+sTxt+'</span>');
    customS.addClass(sIcoHasVal);
}

function initSelects() {
    var select = '.jsSelect';
    $(select).each(function(){
        var s = $(this),
            sItem = s.children('option'),
            sItems = '',
            sClasses = s.attr('class').replace('jsSelect',''),
            sPlaceholder = s.data('placeholder'),
            sLabel = (!checkAttr(s.data('label')))?"":s.data('label'),
            sId = s.attr('id'),
            hasLabel = (sLabel.replace(' ','')!=""),
            hasFixedPlaceholder = checkAttr(s.data('placeholder-fixed')),
            selected = false,   
            sIcoDisabled = (s.attr('disabled') == 'disabled') ? 'select-ico_disabled' : '';
        hasLabel?sLabel='<label class="'+sIcoLabel+'">'+sLabel+'</label>':sLabel='';
        //разбор тегов option
        $(sItem).each(function(){
            var i = $(this);
            if(!checkAttr(i.attr('value'))||(checkAttr(i.attr('value')) && i.attr('value').replace(' ','') == "")){
                i.val(i.html()); //в value то же что и внутри тега
            }
            // console.log('hasSelected: '+checkAttr(i.attr('selected')));
            if(checkAttr(i.attr('selected'))){selected = [sId,i.val()]};
            sItems+='\
                    <li class="'+sIcoListItem+'" data-value="'+i.val()+'"><span class="'+sIcoListText+'">'+i.html()+'</span></li>';
        });
        s.prepend('<option disabled selected></option>'); //заглушка для валидации, .val() выдаст null
        //вкладываем все полученные значения в шаблон
        var sNew = '\
            <div class="'+sIco+' '+sClasses+(hasLabel?' '+sIcoHasLabel:'')+(hasFixedPlaceholder?' '+sIcoHasFixedPlaceholder:'')+' '+sIcoJs+' '+sIcoDisabled+'" data-id="'+sId+'">\
                '+sLabel+'\
                <div class="'+sIcoTitle+'">\
                    <span class="'+sIcoTitleL+'">'+sPlaceholder+'</span>\
    <!-- <svg data-src="'+tplPath+'/img/ico-svg/up-arrow.svg"></svg> -->\
                    <object class="up-arrow-svg" data="'+tplPath+'/img/ico-svg/up-arrow.svg" type="image/svg+xml"></object>\
                </div>\
                <div class="'+sIco+'__height-compensator"></div>\
                <ul class="'+sIcoList+'">\
                    '+sItems+'\
                </ul>\
            </div>\
        ';
        s.before(sNew); //вставляем новый селект
        s.css('display','none'); //скрываем старый селект
        if(selected != false){setSelectItem(selected[0],selected[1])}; //
    });
    initSelectsEvents();
}

function initSelectsEvents() {
    var wrap = '.'+sIcoJs,
        item = '.'+sIcoListText;
    /* выбор варианты */
    $(wrap).click(function(){
        if ( !$(this).hasClass('select-ico_disabled') ){
        $(this).hasClass(sIcoActive)?sClose(this):sOpen(this);
    }
    }).mouseleave(function(){
        sClose(this);
    });
    function sOpen(el) { $(el).addClass(sIcoActive).children("."+sIcoList).slideDown(100); }
    function sClose(el) { $(el).children("."+sIcoList).slideUp(100, function() {$(this).parent().removeClass(sIcoActive)}); }
    //клик по пункту
    $(item).click(function(){
        var i = this,
            par = $(i).closest(wrap),
            parID = par.data('id'),
            tag = i.nodeName.toLowerCase();
        if (tag != 'a') {
            var iVal = $(i).parent().data('value');
            setSelectItem(parID,iVal);
        }
    });
}

//печать документ
$(document).on('click','.js-print', function(event) {
    window.print();
});

// $('.js-input-error-bubble').each(function(){
    // var error = $(this).find('.form__error');
    // var errorCounter = error.length;
    // if (errorCounter > 0){
        // error.wrapAll()
    // }

    // <div class="bubble bubble_100 bubble_bottom bubble_left">
    // <div class="bubble__wrap">



    // </div>
// </div>
// });

// sliders
var sliders = {};

sliders.media = { // max-width
    xl : Infinity,
    lg : 1199,    // large
    md : 991,     // medium
    sm : 767,     // small
    xs : 575,     // extra small
    us : 400      // ultra small
}

sliders.getSets = ($that, sets) => { // sets - custom settings that overwrite defaults
    let a = {
        xl : {
            count : ($that.data('item')) ? $that.data('item') : ($that.data('item-xl')) ? $that.data('item-xl') : 6,
            // width : sliders.media.xl
        },
        lg : { // large <= 1199px
            count : $that.data('item-lg') ? $that.data('item-lg') : 5,
            width : sliders.media.lg
        },
        md : { // medium <= 991px
            count : $that.data('item-md') ? $that.data('item-md') : 4,
            width : sliders.media.md
        },
        sm : { // small <= 767px
            count : $that.data('item-sm') ? $that.data('item-sm') : 3,
            width : sliders.media.sm
        },
        xs : { // extra small <= 575px
            count : $that.data('item-xs') ? $that.data('item-xs') : 2,
            width : sliders.media.xs
        },
        us : { // ultra small <= 400px
            count : $that.data('item-us') ? $that.data('item-us') : 1,
            width : sliders.media.us
        }
    }

    let b = $.extend(true, a, sets);

    return b
}

sliders.checkMedia = () => { // returns lg || md || xm || xs
    var ww = $(window).width();
    var k;

    for(let key in sliders.media) {
        if (ww < sliders.media[key]) {
            k = key;
        }
    }

    // console.log(k);
    return k
}

sliders.getResponsive = (sets) => {
    return [
        {
            breakpoint: sets.lg.width, // large <= 1199px
            settings: {
                slidesToShow: sets.lg.count,
                slidesToScroll: sets.lg.count,
            }
        },
        {
            breakpoint: sets.md.width, // medium <= 991px
            settings: {
                slidesToShow: sets.md.count,
                slidesToScroll: sets.md.count,
            }
        },
        {
            breakpoint: sets.sm.width, // small <= 767px
            settings: {
                slidesToShow: sets.sm.count,
                slidesToScroll: sets.sm.count,
            }
        },
        {
            breakpoint: sets.xs.width, // extra small <= 575px
            settings: {
                slidesToShow: sets.xs.count,
                slidesToScroll: sets.xs.count,
            }
        },
        {
            breakpoint: sets.us.width, // ultra small <= 400px
            settings: {
                slidesToShow: sets.us.count,
                slidesToScroll: sets.us.count,
            }
        }
    ]
}

/*==================== slider gallery  ====================*/
// js-recent-variants
var reсentVsF = { cl : '.js-recent-variants' };
    reсentVsF.cl_par = reсentVsF.cl + '-parent';
    reсentVsF.cl_btns = reсentVsF.cl + '__but-wrap';
    reсentVsF.cl_prev = reсentVsF.cl + '-prev';
    reсentVsF.cl_next = reсentVsF.cl + '-next';
    reсentVsF.cl_item = reсentVsF.cl + '__item';
    reсentVsF.cl_shadow = reсentVsF.cl + '-items-shadow';

var $reсentVs = $(reсentVsF.cl);

reсentVsF.initShadow = function ($that, counter) {
    if ($that.children(reсentVsF.cl_shadow).length === 0) {
        $that.append('<div class="'+reсentVsF.cl_shadow.replace('.js-','')+' ' + reсentVsF.cl_shadow.replace('.','') + '"></div>');
    }

    var $shadowBlock = $that.children(reсentVsF.cl_shadow);

    if (!counter) {
        counter = $that.outerWidth();
    }

    $shadowBlock.outerWidth(counter);

    $that.data('shadow', true);
}

reсentVsF.initSlider = function ({$that, $items, toShow}) {
    var clLastShown = '.slick-last-shown';

    $that.setLastShownClass = function (event, slick, i) {
        let $that = slick.$slider,
            lastShown = i;

        $that.find(clLastShown).removeClass(clLastShown.replace('.',''));
        $that.find('[data-slick-index="'+lastShown+'"]').addClass(clLastShown.replace('.',''));
    };

    $that.one("init", (event, slick) => {
        let i = slick.currentSlide + slick.options.slidesToShow - 1;

        $that.setLastShownClass(event, slick, i);
    });

    let $par = $that.closest(reсentVsF.cl_par);
    $that.slick({
        infinite: false, // don't touch this value infinite: false
        dots: false,
        swipe: false,
        swipeToSlide: false, // если включить работает прокрутка по 1 штуке
        slidesToShow: 4,
        slidesToScroll: 4,
        nextArrow: $par.find(reсentVsF.cl_next),
        prevArrow: $par.find(reсentVsF.cl_prev),
        //responsive: sliders.getResponsive(toShow) для адаптива включить
    });

    $that.on("beforeChange", (event, slick, currentSlide, nextSlide) => {
        let i = nextSlide + slick.options.slidesToShow - 1;

        $that.setLastShownClass(event, slick, i);
    });

    $that.parents(reсentVsF.cl_par).find(reсentVsF.cl_btns).show();

    reсentVsF.initShadow($that);

    $(window).on('resize', function() {
        reсentVsF.initShadow($that)
    });

    $that.on('breakpoint', function () { // Fires after a breakpoint is hit.
        reсentVsF.initShadow($that)
    })

    $that.data('slider', true);
}

reсentVsF.initStatic = function ({$that, $items, toShow}) {
    var F = this;

    F.checkWidth = function() {
        var counter = 0;

        $items.each(function () {
            let $t  = $(this);

            counter += $t.outerWidth(true);
        });

        return counter;
    }

    F.setWidth = function() {
        var counter = F.checkWidth();

        if (counter === 0) {
            $that.hide();
        } else {
            reсentVsF.initShadow($that, counter);
        }
    }

    F.setWidth();

    $(window).on('resize', F.setWidth);

    $that.data('static', true);
}

reсentVsF.init = function(that) {
    var $that  = $(that),
        $items = $that.find(reсentVsF.cl_item),
        $count = $items.length,
        sliderSets = {
            xl : {
                count : 6
            },
            lg : {
                count : 4
            },
            md : {
                count : 3
            },
            sm : {
                count : 2
            },
            xs : {
                count : 1
            }
        },
        sets = sliders.getSets($that, sliderSets),
        toShow = sets[sliders.checkMedia()].count;

    if ($count > toShow){
        reсentVsF.initSlider({
            $that : $that,
            $items : $items,
            toShow : sets
        });
    } else {
        reсentVsF.initStatic({
            $that : $that,
            $items : $items,
            toShow : sets
        });
    }
}

var initReсentVariants = function() {
    $reсentVs.each(function(){
        reсentVsF.init(this);
    });
} 

// js-slider-default
var slidersDF = {
    cl: '.js-slider-default',
    get cl_head() {
        return this.cl + '__header'
    },
    get cl_btns() {
        return this.cl + '__but-wrap'
    },
    get cl_prev() {
        return this.cl + '__prev'
    },
    get cl_next() {
        return this.cl + '__next'
    }
};

var $slidersD = $(slidersDF.cl);

slidersDF.init = function (that) {
    var $that = $(that),
        $header = $that.find(slidersDF.cl_head),
        $btns = $header.find(slidersDF.cl_btns),
        $glry = $that.find('.js-slider-default__gallery'),
        num = $glry.children().length,
        clLastShown = '.slick-last-shown';

    // items to show
    var toShow = sliders.getSets($that);

    // append buttons
    if ($btns.length === 0) {
        $header.append('\
            <div class="'+slidersDF.cl_btns.replace('.js-','')+' '+slidersDF.cl_btns.replace('.','')+'"> \n\
                <button class="el-arrow-but el-arrow-but_big el-arrow-but_prev '+slidersDF.cl_prev.replace('.','')+'"></button> \n\
                <button class="el-arrow-but el-arrow-but_big el-arrow-but_next '+slidersDF.cl_next.replace('.','')+'"></button> \n\
            </div> \n\
        ');

        $btns = $header.find(slidersDF.cl_btns);
    }


    // add last shown class
    $that.setLastShownClass = function (event, slick, i) {
        let $that = slick.$slider, lastShown = i;

        $that.find(clLastShown).removeClass(clLastShown.replace('.',''));
        $that.find('[data-slick-index="'+lastShown+'"]').addClass(clLastShown.replace('.',''));
    };

    $that.one("init", (event, slick) => {
        let i = slick.currentSlide + slick.options.slidesToShow - 1;

        $that.setLastShownClass(event, slick, i);
    });

    let opts = {
        infinite: false,
        dots: false,
        swipe: true,
        swipeToSlide: false, // если включить работает прокрутка по 1 штуке
        slidesToShow: toShow.xl.count,
        slidesToScroll: toShow.xl.count,
        nextArrow: $btns.children(slidersDF.cl_next),
        prevArrow: $btns.children(slidersDF.cl_prev),
        responsive: sliders.getResponsive(toShow) 
    };


    // mods
    if ($that.is('[data-shadow]')) {
        opts.infinite = false;
        opts.swipe = false;
    }

    // init slider

    if (num > toShow[sliders.checkMedia()].count) {
        $glry.slick(opts);

        $that.on("beforeChange", (event, slick, currentSlide, nextSlide) => {
            let i = nextSlide + slick.options.slidesToShow - 1;

            $that.setLastShownClass(event, slick, i);
        });

        $btns.show();
    }
}

var initSliderDefault = function() {
    $slidersD.each(function () {
        slidersDF.init(this);
    });
}

var initSliderHelp = function() {
    $('.js-slider_help').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToScroll: 1,
        cssEase: 'linear',
        //  variableWidth: true
    }).css('visibility','visible');
}

var initSliderArrows = function() {
    $('.js-slider_arrows').slick({
        infinite: true,
        dots: false,
        arrows: true,
        slidesToShow: 6,
        slidesToScroll: 6,
        // centerMode: true,
        variableWidth: false
    }).css('visibility','visible');
}

var initSliderFade = function () {
    $('.js-slider_fade').slick({
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        fade: true,
        autoplay: true,
        autoplaySpeed: 4000,
        cssEase: 'linear',
        //чтобы не было скролла к слайдеру
        focusOnSelect: false,
        accessibility: false,
        centerMode: false
  }).css('visibility','visible');
}

var initSliders = function() {
    initReсentVariants();
    initSliderDefault();
    initSliderHelp();
    initSliderArrows();
    initSliderFade();
}

sliderRangeRefresh();
$( function() {
    $('#datepicker').datepicker({
        inline: true,
        firstDay: 1,
        showOtherMonths: true,
        // stepMonths: 12,
        dayNamesMin: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
        monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
    });

    $('.variants-count__num_0').parent().hide();
    $('.image-block .left-side').css('visibility','visible');
});

window.jsInputSpinRefresh = () => {
    jQuery(".js-input-spin").each(function(){
        var $that = jQuery(this);
        var min = (jQuery.inArray( $that.attr('min'), [undefined, '']) < 0 ? parseFloat($that.attr('min')) : 1);
        var step = (jQuery.inArray( $that.attr('step'), [undefined, '']) < 0 ? parseFloat($that.attr('step')) : 1);
        var maxboostedstep = (step > 1) ? 1 : 10;
        var unit = (jQuery.inArray( $that.attr('unit'), [undefined, '']) < 0 ? parseFloat($that.attr('unit')) : '');
        var decimals = (step >= 1 ? 0 : 2);

        //console.log(min, step, unit);
        $that.TouchSpin({
            min: min,
            max: 99999,
            step: step,
            decimals: decimals,
            boostat: 5,
            maxboostedstep: maxboostedstep,
            postfix: unit,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
        });

        $that.css('display', 'inline-block');
    });
    jQuery('.el-spinner').each(function(){
        var min = (jQuery.inArray( jQuery(this).attr('min'), [undefined, '']) < 0 ? parseFloat(jQuery(this).attr('min')) : 1);
        var step = (jQuery.inArray( jQuery(this).attr('step'), [undefined, '']) < 0 ? parseFloat(jQuery(this).attr('step')) : 1);
        var unit = (jQuery.inArray( jQuery(this).attr('unit'), [undefined, '']) < 0 ? parseFloat(jQuery(this).attr('unit')) : '');
        //console.log(min, step, unit);
        var spinner = $(this).spinner({
            min: min,
            max: step * 1000,
            step: step
        });
    });
}


// карточка товара, открытие дополнительных блоков под табами из атрибута data-add-block
function addBlockForTab(blocks) {
    var blocksArr = blocks.replace(' ','').split(','); //делим на массив
    $('.data-add-block:not('+blocks+')').animate({opacity:0},0,function(){this.style.display='none'});//скрываем все блоки кроме наших
    blocksArr.forEach(function(block){
        $(block).css('display','block').animate({opacity:1},300);//показываем наши блоки
    });
}


// выпадающие блоки как в фильтрах листинга
var initJsSlide = function () {
    var F = {
        cl : {
            slide: '.js-slide',
            get parent() {
                return this.slide + '-parent'
            },
            closed: 'closed'
        }
    };

    var $slides = $(F.cl.slide);


    F.update = function ($that, delay) {
        var $par = $that.parents(F.cl.parent),
            $cont = $that.next(),
            d = delay ? delay : [0, 0];

        if ($par.hasClass(F.cl.closed)){
            $cont.slideUp(d[0]);
        } else {
            $cont.slideDown(d[1]);
        }
    }

    F.change = function ($that) {
        var $par = $that.parents(F.cl.parent);
        $par.toggleClass(F.cl.closed);
    }

    for (let i = 0; i < $slides.length; i++) {
        let $slide = $slides.eq(i);
        F.update($slide);

        $slide.on('click', function(e) {
            if (e.target.tagName != 'A'){
                let $that = $(this);
                F.change($that);
                F.update($that, [200, 300]);
            }
        });
    }
}

// sitemap functions
function smStart(map, parent) {
    smHider(map, parent);
}

function smHider(map, parent) {
    var level = '-level-';
    for(var i = 1; true; i++) {
        var mapItem = parent + ' ' + map+level+i + ' > li ';

        if ($(mapItem).length > 0) { // checkout for exist
            $(mapItem).each(function(){
                var that = $(this),
                    mapItemChild = that.children('ul');
                that.addClass(map.replace('.','')+'-item');
                if(mapItemChild.length > 0) {
                    that.children('a').attr('title','Открыть список');
                    that.children('a').after('<span class="'+map.replace('.','')+'-item-toggler" ></span>');
                    that.addClass(map.replace('.','')+'-item_has-child');
                }
            });
        } else {
            $(map+'-item_has-child > a').on('click',function(event){
                var that = $(this),
                    parent = that.parent();
                console.log('clicked');
                if(!parent.hasClass('active')) {
                    event.preventDefault();
                    parent.addClass('active');
                    parent.children('ul').show('fast');
                    that.attr('title','Перейти в каталог');
                }
            });
            $(map+'-item_has-child > '+map+'-item-toggler').on('click',function(){
                var that = $(this),
                    parent = that.parent();
                console.log('toggler clicked');
                if(parent.hasClass('active')) {
                    console.log('toggler active clicked');
                    parent.removeClass('active');
                    parent.children('ul').hide('fast');
                    parent.children('a').attr('title','Открыть список');
                } else {
                    parent.addClass('active');
                    parent.children('ul').show('fast');
                    parent.children('a').attr('title','Перейти в каталог');
                }
            });
            break;
        };
    };
}
//
var changeFixedHeader = (sets) => {
    if (sets.offset < $(window).scrollTop()) {
        sets.header.addClass(sets.fixClass);
    } else {
        sets.header.removeClass(sets.fixClass);
    }
}

var initFixedHeader =() => {
    // if ($("#header-append-container").length > 0) {
    //     var container   = $("#header-append-container"),
    //         header      = $("#header-fixed"),
    //         initClass   = 'header-fixed_initialized',
    //         fixClass    = 'header-fix'

    //     if (container.hasClass(initClass)) {
    //         changeFixedHeader({
    //             header   : header,
    //             offset   : container.data('offsetTop'),
    //             fixClass : fixClass
    //         });
    //     } else {
    //         var offset = container.offset().top;
    //         container.data('offsetTop', offset).addClass(initClass);
    //         changeFixedHeader({
    //             header   : header,
    //             offset   : offset,
    //             fixClass : fixClass
    //         });
    //     }
    // }
}

// recognizes path to script file
// path type can be
// "file"(file name)
// "path"(path without file)
// empty(full path)
function ScriptPath(pathType) {
    try {
        //Throw an error to generate a stack trace
        throw new Error();
    } catch (e) {
        //Split the stack trace into each line
        var stackLines = e.stack.split('\n');
        var callerIndex = 0;
        //Now walk though each line until we find a path reference
        for (var i in stackLines) {
            if (!stackLines[i].match(/http[s]?:\/\//)) continue;
            //We skipped all the lines with out an http so we now have a script reference
            //This one is the class constructor, the next is the getScriptPath() call
            //The one after that is the user code requesting the path info (so offset by 1)
            callerIndex = Number(i) + 1;
            break;
        }
        //Now parse the string for each section we want to return
        pathParts = stackLines[callerIndex].match(/((http[s]?:\/\/.+\/)([^\/]+\.js))/);
    }

    this.fullPath = function() {
        return pathParts[1];
    };

    this.path = function() {
        return pathParts[2];
    };

    this.file = function() {
        return pathParts[3];
    };

    this.fileNoExt = function() {
        var parts = this.file().split('.');
        parts.length = parts.length != 1 ? parts.length - 1 : 1;
        return parts.join('.');
    };

    switch (pathType) {
        case "file":
            return pathParts[3];
            break;
        case "path":
            return pathParts[2];
            break;
        default:
            return pathParts[1];
    }
}
//

var isViewed = function (target) {
    // Все позиции элемента
    var targetPosition = {
        top: window.pageYOffset + target.getBoundingClientRect().top,
        left: window.pageXOffset + target.getBoundingClientRect().left,
        right: window.pageXOffset + target.getBoundingClientRect().right,
        bottom: window.pageYOffset + target.getBoundingClientRect().bottom
    },
    // Получаем позиции окна
    windowPosition = {
        top: window.pageYOffset,
        left: window.pageXOffset,
        right: window.pageXOffset + document.documentElement.clientWidth,
        bottom: window.pageYOffset + document.documentElement.clientHeight
    };

    if (targetPosition.bottom > windowPosition.top && // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
    targetPosition.top < windowPosition.bottom && // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу
    targetPosition.right > windowPosition.left && // Если позиция правой стороны элемента больше позиции левой части окна, то элемент виден слева
    targetPosition.left < windowPosition.right) { // Если позиция левой стороны элемента меньше позиции правой чайти окна, то элемент виден справа
    // Если элемент полностью видно, то запускаем следующий код
        return true
    } else {
    // Если элемент не видно, то запускаем этот код
        return false
    };
};

function getScrollTop (el) {
    if (el) {
        var top;

        if (el instanceof HTMLElement) {
            top = el.getBoundingClientRect().top;
        } else if (el instanceof jQuery) {
            top = el.offset().top;  
        } else {
            console.error(el , 'isn\'t a jQuery or HTML element');
            return false;
        }

        return top;
    } else {
        if(typeof pageYOffset!= 'undefined'){
            //most browsers except IE before #9
            return pageYOffset;
        } else {
            var B = document.body; //IE 'quirks'
            var D = document.documentElement; //IE with doctype
            D = (D.clientHeight) ? D : B;
            return D.scrollTop;
        }
    }
}

function getScrollBot (el) {
    if (el) {
        var outerH;

        if (el instanceof HTMLElement) {
            outerH = el.getBoundingClientRect().height;
        } else if (el instanceof jQuery) {
            outerH = el.outerHeight();
        } else {
            console.error(el , 'isn\'t a jQuery or HTML element');
            return false;
        }

        var top = getScrollTop(el);

        return document.body.scrollHeight - top - outerH;
    } else {
        var top = getScrollTop();
        return document.body.scrollHeight - document.documentElement.clientHeight - top;
    }
}


var slideTopDownEngine = function ($target, $main){
    if ($main.length === 1 ){
        let headerH = $('#header-fixed[data-height-fix]') ? parseInt(document.getElementById('header-fixed').dataset.heightFix) : 0,
            topOffset = headerH + 10,
            jsSlideTop = getScrollTop($main) - topOffset,
            excludeEls = $('.js-slide-exclude');
            console.log(headerH);  
        var $trgH = function () { //высота виджета
            return ($target.height() - excludeEls.outerHeight(true));
        }

        var jsSlideBottom = function () { // делаем функцией так как высота $target может меняться
            return jsSlideTop + $main.height() - $trgH();
        }

        var checkCondition = function () {
            return (
                $(window).height() >= $trgH() &&
                $main.height() > $trgH() &&
                $(window).width() > 1280 &&
                isViewed($main[0])
            );
        }

        var resetFix = function () {
            if (checkCondition()) {
                if ($main.height() > $trgH()){
                    var scrollTop = getScrollTop();

                    // console.log($(window).height(), jsSlideTop, jsSlideBottom());

                    var condFix = scrollTop >= jsSlideTop,
                        contTop = scrollTop < jsSlideTop, 
                        condBot = scrollTop > jsSlideBottom();

                    if (condFix) { // fixed
                        $target.addClass('fixed').css('top', topOffset);
                    } else if (contTop) { // top pos
                        $target.removeClass('fixed').css('top', 0);
                    };

                    if(condBot) { // bottom pos
                        $target.removeClass('fixed').css({
                            'top' : jsSlideBottom() - jsSlideTop,
                        });

                        setTimeout(function () {
                            if (+$target.css('top').replace('px','') !== jsSlideBottom() - jsSlideTop) {
                                resetFix(); // запускаем повторно если позиция изменилась
                            }
                        }, 25);
                    }
                }
            } else {
                $target.removeClass('fixed').css('top', 0);
            }
        }

        resetFix();

        $(window).scroll(resetFix).resize(resetFix);
        $target.click(resetFix);
        $main.click(resetFix);
        $(document).on('click', '.js-slide-click', function() { 
            setTimeout(function() { resetFix() }, 500)
        });
    }
};

var slideTopDown = function () {
    var $target = $('#js-slide-trg'),
        $main = $('#js-slide-main');

    slideTopDownEngine($target, $main);
}

//});

// bonus steps function
// hoverActive({
//     parent : '.b-info-bonus-steps',
//     item   : '.b-info-bonus-steps__item',
//     toShow : '.b-info-bonus-steps__box'
// });
//
// add ID classes! for example
// b-info-bonus-steps__item_ID1
// b-info-bonus-steps__box_ID1
window.hoverActive = function (F) { // add _active class to toShow class
    var parent = F.parent, // parent class
        item = F.item, // hover item
        toShow = F.toShow, // to show
        reg = new RegExp(item.replace('.',''),'g'), //reg
        active = toShow.replace('.','') + '_active';

    // hover
    $(document).on('mouseenter', item, function(){
        var $that = $(this);

        // класс элемента который нужно показать
        var toShowNew = toShow + $that.attr('class').replace(reg,'').replace(' ','');

        // родительский блок через который идет поиск элементов
        var $parent = $that.closest(parent);

        // убираем/добавляем активный класс
        if (!$parent.find(toShowNew).hasClass(active)) {
            $parent.find(toShow).removeClass(active);
            $parent.find(toShowNew).addClass(active);
        };
    });
}

// подключение кастомных скроллов
// include(tplPath + '/js/jquery.scrollbar.min.js');
// SVGInjector 
$( document ).ready(function() { 
    new SVGInjector().inject(document.querySelectorAll('svg[data-src]:not([role])'));
});

// onready
jQuery(document).ready(function(){
    jsInputSpinRefresh();
    labelTopRefresh();
    initFixedHeader();
    initSliders();
    initJsSlide();
    slideTopDown();
    
    // sitemap
    var prefix = '.map'; //
    var wrapper = prefix+'-columns'; //
    smStart(prefix, wrapper); //sitemap init

    // инициализация addBlockForTab
    $(document).on('click','[data-add-block]',function(){
        addBlockForTab($(this).attr('data-add-block'));
    });

    $('.js-copy-text').on('click', function(){
        copyToClipboard('#js-copy-text__target');
    })
});

// onload
jQuery(window).load(function() { 
    $( document ).ajaxComplete(function() {
        new SVGInjector().inject(document.querySelectorAll('svg[data-src]:not([role])'));

        //Если контент со скроллом загружается аяксом
        jQuery('.nav-catalogue_two .scrollbar-inner').scrollbar({
            disableBodyScroll: true
        });
    });

    $('.js-cstm-scroll, .lightbox__content').perfectScrollbar();

    // инициализация кастомных скроллов
    jQuery('.nav-catalogue > .scrollbar-inner, .scrollbar, .scrollbar-macosx, .scollbar-textarea').scrollbar({
        disableBodyScroll: true
    });
    jQuery('.filters__content > .scrollbar-inner').scrollbar({
        'onScroll' : function (event){ //js-scroll-fix
            //var posLast =
            clearTimeout($.data(this, 'timer'));
            $.data(this, 'timer', setTimeout(function(){ 
                let el = $(this)["0"].container, 
                    elHeight = el.find('.js-scroll-fix-el').eq(1).outerHeight(true),
                    posTop = el.scrollTop();
                /* Если после прокрутки высоту всех элементов поделить на высоту одного и получиться остаток, 
                значит верхний элемент влезает не полностью и нужно докрутить чтобы остатка не было */
                while( posTop % elHeight > 0 ) { 
                    posTop++;
                    el.scrollTop(posTop); 
                    console.log( posTop);
                };
            }.bind($(this)),250));
            
        }
    });
});

// onscroll
$(window).scroll(function() {
    initFixedHeader();

    // кнопка наверх
    // Высота проявления кнопки
    if ($(this).scrollTop() > 100) {
        $('#go-to-top').fadeIn();
    } else {
        $('#go-to-top').fadeOut();
    }

}); 

$('.form__item input:required').each(function(){
    var wrap = $(this).parents('.form__item');
    var label = wrap.children('label');
    var required = ' <span class="el-marked"> *</span>';
    var id = $(this).attr('name');
    var name = label.text();

    $(required).appendTo(label);
    var html = '<label for="' + id + '" class="el-placeholder">' + name + required +'</label>';
    $(this).attr({'id': id}).addClass('placeholder-empty');
    $(this).after(html);
})

// Я хз что это но оно ломает js так как конструктора PS нет!!!
/* const containerCity = document.querySelector('.ps-container-city');
Ps.initialize(containerCity, {
  suppressScrollX: true,
});


const containerOblast = document.querySelector('.ps-container-oblast');
Ps.initialize(containerOblast, {
  suppressScrollX: true,
});

const containerChoiseCity = document.querySelector('.ps-container-choise-city');
Ps.initialize(containerChoiseCity, {
  suppressScrollX: true,
}); */


// const popUp = new PopUp(
//     document.querySelector('.outh')
// )
// popUp.init();






