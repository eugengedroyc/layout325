class dynamicTimer {
	constructor(func, delay) {
		this.callbacks = [func];
		this.triggerTime = +new Date + delay;
		this.timer = 0;
		this.updateTimer(); 
	}

	updateTimer() {
		let that = this;
		clearTimeout(that.timer);

		let delay = that.triggerTime - new Date;

		this.timer = setTimeout(function () {
			for (let i = 0; i < that.callbacks.length; i++) {
				that.callbacks[i]();
			}

			that.resetTimer();
		}, delay);

		return this
	}

	setTime(delay) {
		this.triggerTime = +new Date + delay;
		this.updateTimer();
		return this
	}

	addTime(delay) {
		this.triggerTime += delay;
		this.updateTimer();
		return this
	}

	setFunc(func) {
		this.callbacks = [func];
		this.updateTimer();
		return this
	}

	addFunc(func) {
		this.callbacks.push(func);
		this.updateTimer();
		return this
	}

	add(func, delay) {
		this.triggerTime += delay;
		this.callbacks.push(func);
		this.updateTimer();
		return this
	}

	resetTimer() {
		clearTimeout(this.timer);
		this.callbacks = [];
		this.triggerTime = 0;
		this.timer = 0;

		return this
	}
}

// (function () {
	
// let timer = new dynamicTimer(function() {
// 	console.log("2+2 =", 2 + 2)
// }, 50) // Должно сработать почти мгновенно

// timer.addTime(1000) // Но мы продлим ещё на секунду

var navLightbox = '.nav-lightbox',
	nav = '.nav-catalogue', // класс каталога
	nav_1 = nav + '_1',
	navToggle = '.js-menuopen',
	navParr = nav + '-parent',
	navParr_y = navParr + '_y',
	navWrap = nav + '-wrap',
	navWrap_1 = navWrap + '_1',
	navWrapInit = navWrap + '_init',
	navWrapBg = navWrap + '__background',
	navWrapBgInit = navWrapBg + '_init',
	navDisplayToggle = navWrapBg, // главная обертка меню (та которую будем переключать)
	navOpenDelay = 150, // задержка включения меню
	navCloseDelay = 150;  // задержка отключения меню
	// nav1lvlMaxItems = false; // ограничение высоты левого меню, сейчас всего 9 пунктов, false - не ограничивать

    var $nav = {
	// FIn  : 80,
	// FOut : 80,
	delay: 0, // default 80 | значение 0 отключает задержку и генерацию таймеров
	on: navWrap.replace('.','') + '_on',
	off: navWrap.replace('.','') + '_off'
};

$(document).ready(function(){

	$nav.deactivateTimeout = { // будущий dynamicTimer
		timer : 0
	};

	$(nav_1).menuAim({
		firstActiveRow: true,
		activate: function(a) {
			// console.time('activate');
			$nav.activate = function () {
				// добавляем классы js-lastopen
				$(a).addClass('js-lastopen'); // что бы оставался ховер эфект
				// переключаем активные классы содержимого
				$(a).children(navWrap).removeClass($nav.off).addClass($nav.on);
				setNavMenuHeight( $(a).children(navWrap));
				// console.timeEnd('activate');
			}

			if ($nav.delay > 0) { // проверка на наличие задержки
				if ($nav.activateTimeout) clearTimeout($nav.activateTimeout);

				$nav.activateTimeout = setTimeout($nav.activate, $nav.delay);
			} else {
				$nav.activate();
			}
		},
		deactivate: function(a) {
			// console.time('deactivate');
			var timerEmpty = $nav.deactivateTimeout.timer === 0;

			$nav.deactivate = function () {
				// удаляем классы js-lastopen
				$(a).removeClass('js-lastopen').find(nav+'__item-wrap').removeClass('js-lastopen');
				// переключаем активные классы содержимого
				$(a).children(navWrap).removeClass($nav.on).addClass($nav.off);
				setNavMenuHeight( $(a).children(navWrap) );
				// console.timeEnd('deactivate');
			}

			if ($nav.delay > 0) { // проверка на наличие задержки
				if (timerEmpty) {
					$nav.deactivateTimeout = new dynamicTimer($nav.deactivate, $nav.delay)
				} else {
					$nav.deactivateTimeout.setTime($nav.delay).addFunc($nav.deactivate);
				}
			} else {
				$nav.deactivate();
			}
		},
		exit: function(a) {
			//проверяет есть ли курсор над какой то из менюшек, если нет то закрываем все пункты меню
			//закоментить условие если нужно что бы меню было на том же месте на котором закрылось
			if (!($('header.header:hover').length > 0)) {
				$(a).closest(navDisplayToggle).hide();
				$(a).closest(navToggle).removeClass('active');
				setNavMenuHeight($(a));
			}
		}
	});

	menuMouseEvents();
});

// инициализация ивентов
function menuMouseEvents() {
	var cl_body_open = 'body-' + nav.replace('.','') + '-open'; // .body-nav-catalogue-open
	var jt = {};

	// закрытие меню
	jt.hide = function ($jt) {
		// проверка на наличие класса открытого меню
		// if ($('body').hasClass(cl_body_open)) { // если меню открыто то зарываем
			$jt.find(navDisplayToggle).css('display', 'none'); // выключаем обертку
			$jt.removeClass('active'); // убираем активный класс
			setNavLightbox("off"); // отключаем лайтбокс
			$('html').removeClass(cl_body_open).removeClass(cl_body_open + '_scroll'); // показываем остальным функциям что меню закрыто
		// } else {
		// 	return false;
		// }
	}

	// открытие меню
	jt.show = function ($jt) {
		// console.time('jtShow');
		// if ($('body').hasClass(cl_body_open)) { // проверка на наличие класса открытого меню
			// return false;
		// } else { // если меню закрыто то открываем
			$jt.find(navDisplayToggle).css('display', 'block'); // включаем обертку
			$jt.addClass('active'); // добавляем активный класс
			setNavLightbox("on"); // включаем лайтбокс

			var hh = $('header.header').eq(0).outerHeight() + $(nav_1).eq(0).outerHeight(), // header height
				wh = $(window).height() // window height
			
				if ($('#bx-panel').length > 0) hh = hh + $('#bx-panel').outerHeight();

			if (wh > hh) {
				$('html').addClass(cl_body_open); // показываем остальным функциям что меню открыто
			} else {
				$('html').addClass(cl_body_open + ' ' + cl_body_open + '_scroll'); // показываем остальным функциям что меню открыто
			}

			// функции инициализации
			initNav1();
			initNavBg();
			initNav2();
			setNavMenuHeight_y();
		// }
		// console.timeEnd('jtShow');
	}

	// обрабатываем ивенты
	$(navToggle).on('mouseenter', function(){
		if(!$('body').hasClass('is-touchscreen')){
			$jt = $(this);

			// общий таймаут на открытие и установку события на закрытие
			var jtEnterTimeout = setTimeout(function () {
				jt.show($jt);
				$('.header-info').hide(); 
			}, navOpenDelay);

			$jt.one('mouseleave', function (){
				clearTimeout(jtEnterTimeout);
			});
		}
	});

	$(navToggle).on('mouseleave', function (){ //закомментить чтобы не скрывалось для девелоп-версии
		$jt = $(this);

		var jtOutTimeout = setTimeout(function () {
			jt.hide($jt);
			$('.header-info').show();
		}, navCloseDelay);

		$jt.one('mouseenter', function (){
			clearTimeout(jtOutTimeout);
		});
	});
}

// инициализация первого уровня меню
/*
var initNav1Slider = function () {
	var n = {};
	n.nav1 = $(nav_1);
	n.maxItems = nav1lvlMaxItems;
	n.data_init = ['slider','inited'];

	if (n.maxItems && n.nav1.data(n.data_init[0]) != n.data_init[1] ) {
		n.nav1.data('start', 0); // начальная позиция слайдера
		n.nav1Items = n.nav1.children();
		n.cl_visible = nav + '_visible';
		n.cl_visibleFirst = nav + '_visible-first';
		n.cl_visibleLast = nav + '_visible-last';

		function setHeightData () {
			n.nav1Items.each(function() {
				$(this).data('height', $(this).outerHeight(true));
			});
		}

		function resetClasses() {
			var max = n.maxItems + n.nav1.data('start');
			var iii = n.nav1.data('start');

			for (var i = 0; i <= n.nav1Items.length; i++) {
				n.nav1Items.eq(i).removeClass(n.cl_visible.replace('.','') + ' ' + n.cl_visibleFirst.replace('.','') + ' ' + n.cl_visibleLast.replace('.',''));

				if (i >= iii && i < max) {
					n.nav1Items.eq(i).addClass(n.cl_visible.replace('.',''));

					if (i === iii) {
						n.nav1Items.eq(i).addClass(n.cl_visibleFirst.replace('.',''));
					}

					if ((i + 1) == max) {
						n.nav1Items.eq(i).addClass(n.cl_visibleLast.replace('.',''));
					}
				}
			}
		}

		function countMaxItemsHeight() {
			var counter = 0;

			for (var i = n.nav1.data('start'); i < n.maxItems; i++) {
				counter += n.nav1Items.eq(i).data('height');
				console.log(n.nav1Items.eq(i).data('height'));
			}

			return counter;
		}

		function cut () {
			function setMaxItemsHeight() {
				var counter = Number(n.nav1.css('padding-top').replace('px','')) + Number(n.nav1.css('padding-bottom').replace('px','')) + countMaxItemsHeight();
	
				n.nav1.css('max-height', counter);
			}

			setMaxItemsHeight();
		}

		function changePos(stat) {
			if (stat == 'minus' || stat == 'plus') {
				var count;

				if (stat == 'minus') {
					var $prev = n.nav1.children(n.cl_visibleFirst).prev();
					if ($prev.length > 0) {
						var h = 0;
						
						if ($prev.prevAll().length > 0) {
							$prev.prevAll().each(function () {
								h += $(this).data('height');
							});
						}
						n.nav1.css('top', -h);
						$(navWrap+"_2.scroll-wrapper").css('top', h);
						count = n.nav1.data('start') - 1;
					};
				} else if (stat == 'plus') {
					var $prev = n.nav1.children(n.cl_visibleFirst).prev();
					var $next = n.nav1.children(n.cl_visibleLast).next();
					if ($next.length > 0) {
						var h = 0;

						if ($prev.prevAll().length > 0) {
							$prev.prevAll().each(function () {
								h += $(this).data('height');
							});
						}

						h += n.nav1.children(n.cl_visibleFirst).data('height');
						n.nav1.css('top', -h);
						$(navWrap+"_2.scroll-wrapper").css('top', h);
						count = n.nav1.data('start') + 1;
					}
				}

				n.nav1.data('start', count);
				resetClasses();
			}
		}

		function watch() {
			n.nav1Height = n.nav1.outerHeight();
			n.nav1Top = n.nav1.offset().top;
			// delay
			var delay = 200;
			var delayBlock = false;
			// var to;

			n.nav1.on('mousemove', function (e) {
				// var x = (e.offsetX === undefined) ? e.layerX : e.offsetX;
				// var y = (e.offsetY === undefined) ? e.layerY : e.offsetY;
				var top = e.clientY - n.nav1Top; // cursor top pos
				var bot = n.nav1Height - top; // cursor bottom pos
				// console.log('top: '+top+'\nbot: '+bot);
				if (top < 50) {
					if (delayBlock !== 'minus') {
						delayBlock = 'minus';
						var to = setTimeout(function () {
							changePos('minus');
							delayBlock = false;
						}, delay);
					}
				} else if (bot < 50) {
					if (delayBlock !== 'plus') {
						delayBlock = 'plus';
						var to = setTimeout(function () {
							changePos('plus');
							delayBlock = false;
						}, delay);
					}
				}

				n.nav1.one('mouseleave', function() {
					console.log('mouseleave');
					clearTimeout(to);
					delayBlock = false;
				});
			});
		}

		setHeightData();
		cut();
		resetClasses();
		watch();
		n.nav1.data(n.data_init[0],n.data_init[1]);
	}
}
*/

function setNav1Pos() {
	var $navWrap = $(navWrap_1).eq(0);
	var leftOffset = $(navToggle).offset().left;

	$navWrap.css('left', leftOffset);
}

function initNav1() {
	var $navWrap = $(navWrap_1).eq(0);
	setNav1Pos();
	// initNav1Slider();

	if (!$navWrap.data('init')) {
		$(window).on('resize', setNav1Pos);
		$navWrap.data('init', true);
	}
}

// инициализация второго уровня меню
function setNav2Width(F) {
	var menuW = F.menu.outerWidth(),
		nav1W = F.nav1.outerWidth();

	F.nav2.css('width', menuW - nav1W);
}

function initNav2() {
	var F = {
		init : navWrapInit.replace('.', ''),
		menu : $('#header-fixed > .wrapper'),
		nav1 : $(navWrap_1).eq(0),
		nav2 : $(navWrap+'_2')
	}

	setNav2Width(F);

	// if not initialized
	if (!F.nav2.hasClass(F.init)) {
		$(window).on('resize', function () {
			setNav2Width(F);
		});

		F.nav2.addClass(F.init);
	}
}

// инициализация бэкграунда
function setNavBgPos() {
	// устанавливаем отрицательный отступ слева от каталога 
	$(navWrapBg).css('left', Number('-'+$(navToggle).offset().left));
}

function setNavBgHeight() {
	// height
	$(navWrapBg).css('height', $(nav_1).outerHeight());
}

function setNavBgWidth() {
	// width
	$(navWrapBg).css('width',$(window).width());
}

function initNavBg() {
	if (!$(navWrapBg).hasClass(navWrapBgInit.replace('.',''))) {	
		setNavBgHeight();
		setNavBgWidth();
		setNavBgPos();

		$(window).on('resize', function () {
			setNavBgPos();
			setNavBgWidth();
		});
	}
}

// переключатель лайтбокса. передавать в stat строку "on" или "off"
function setNavLightbox(stat) {
	var active = navLightbox.replace('.','') + '_active';

	if (stat == "on") {
		$(navLightbox).addClass(active);
	} else if (stat == "off") {
		$(navLightbox).removeClass(active);
	}
}

// that is unnecessary
// динамичесское выравнивание высоты, кроме _y модификатора
function setNavMenuHeight(that) {
	var n = {};

	n.parr  = that ? that.closest(navParr) : $(navParr);

	if (!n.parr.hasClass(navParr_y.replace('.',''))) {
		n.wrap  = n.parr.find(navWrap+':visible');   //все видимые обертки
		n.wrap1 = n.parr.find(navWrap_1).eq(0); // обертка секций
		n.wrap2 = n.parr.find(navWrap+'_2:visible');  // обертка категорий

		//обнуляем высоту
		n.wrap.height('auto');

		var minheight = n.wrap1.children(nav).outerHeight(), // высота секций каталога считаеться минимальной
			contheight = 0;

		n.wrap2.children().each(function() {
			// суммируем высоту дочерних элементов
			contheight += $(this).outerHeight(true);
		});

		minheight = (contheight > minheight) ? contheight : minheight;

		n.wrap.height(minheight);
	}
}

// выравнивание высоты под левый блок только для _y модификатора
function setNavMenuHeight_y(that) {
	var n = {};
	n.parr  = that ? that.closest(navParr) : $(navParr);

	if (n.parr.hasClass(navParr_y.replace('.',''))) {
		var initClass = navParr.replace('.','') + '_wrappers-init';
		if (!$(n.parr).hasClass(initClass)) {
			n.wrap  = n.parr.find(navWrap);   // все обертки
			n.wrap1 = n.parr.find(navWrap_1).eq(0); // обертка секций
			n.wrap1.outerH = n.wrap1.children(nav).outerHeight(); // высота секций каталога считаеться минимальной

			n.wrap.height(n.wrap1.outerH);
			$(n.parr).addClass(initClass);
		}
	}
}

// navFixF.change
var navFixF = {} // все что касается закрепления меню

navFixF.other = function (sets, stat) { // дополнительные функции при закреплении\откреплении
	var open = function () {};

	var close = function () {};

	var both = function () {
		// CheckTopMenuOncePadding();
		// sets.header.find('.mega-menu.sliced.initied').removeClass('initied'); // нужно для CheckTopMenuDotted
		// CheckTopMenuDotted();
		// initNavBg();
	};

	if (stat === 'open') {
		open();
		both();
	} else if (stat === 'close') {
		close();
		both();
	} else {
		open();
		close();
		both();
	};
}

navFixF.fix = function (sets) { // закрепление меню
	// let delay = 300;

	sets.header
		.addClass(sets.isChangingIn)
		.addClass(sets.fixedClass);
	// 	.css('animation-duration', delay + 'ms');

	// setTimeout(function () {
		sets.header
			.data('fixed', true)
			.removeClass(sets.isChangingIn);
	// }, delay);

	navFixF.other(sets, 'open'); // обновляем плашку пока она открывается.
};

navFixF.unfixImm = function (sets) { // немендленное открепление
	sets.header
		.removeClass(sets.isChangingOut)
		.removeClass(sets.fixedClass)
		.removeClass(sets.isChangingIn)
		.data('fixed', false);

	navFixF.other(sets, 'close');
}

navFixF.unfix = function (sets) { // открепление меню
	// let delay = 100;

	if (sets.header.data('fixed') === true) {
	sets.header
	// 	.css('animation-duration',  delay + 'ms')
		.addClass(sets.isChangingOut);

	// setTimeout(function () {
		sets.header
			.removeClass(sets.isChangingOut + ' ' + sets.fixedClass)
			.data('fixed', false);

		navFixF.other(sets, 'close');
	// }, delay);
	}
}

navFixF.change = function (sets) {
	var st = $(window).scrollTop();	

	// update
	if (sets.offset <= st) { // если проскроллили sets.offset то закрепляем меню
		if (!sets.header.hasClass(sets.fixedClass)) {
			navFixF.fix(sets);
		}
    } else if (sets.offset > st && sets.header.hasClass(sets.fixedClass)) {
		navFixF.unfix(sets); // открепляем с анимацией если не поднялись выше верхней позиции 
	} else if (sets.header.hasClass(sets.fixedClass)) { // проверка на наличие класса _fixed
		navFixF.unfixImm(sets); // открепляем незамедлительно если поднялись выше
	}
}

navFixF.init = function (stat) {
	var container = $("#header-append-container");

    if (container.length > 0) {
        var sets = {
			header      : $("#header-fixed"),
            initClass   : 'header-fixed_initialized',
			fixedClass  : 'header-fixed_fixed',
			isChangingIn  : 'header-fixed_isChangingIn',
			isChangingOut  : 'header-fixed_isChangingOut'
		}

		this.Resize = function () {
			// var ww = window.outerWidth;

			// if (window.outerWidth > content.)
			// sets.header.css({
			// 	'width': ww
			// });

			// $('header.header').css({
			// 	'width': ww
			// });

			// $('html').css({
			// 	'width': ww,
			// 	'overflow-x': 'hidden'
			// });
		}

		if (stat === "resize") { // onresize
			this.Resize();
		} else if (container.hasClass(sets.initClass)) { // update
			sets.offset = container.data('offsetTop');
      navFixF.change(sets);
		} else { // initialize
			sets.offset = container.offset().top;

			//container.height(container.height());
			this.Resize();
			container.data('offsetTop', sets.offset).addClass(sets.initClass);
          navFixF.change(sets);
        }
    }
}


$(document).ready(function () {
	navFixF.init();
});

$(document).on('scroll', function () {
	navFixF.init();
})

$(window).on("resize", function () {
	navFixF.init("resize");
})

// })()