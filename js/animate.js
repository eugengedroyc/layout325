window.animate = function({timing, draw, duration, end}) {
    let start = performance.now();

    requestAnimationFrame(function animate(time) {
		// timeFraction goes from 0 to 1
		let timeFraction = (time - start) / duration;
		if (timeFraction > 1) timeFraction = 1;

		// calculate the current animation state
		let progress = timing(timeFraction);

		draw(progress); // draw it

		// console.log(start);

		if (timeFraction < 1) {
			requestAnimationFrame(animate);
		} else {
			end();
		}
    });
};

window.animateMakeEaseOut = function (timing) {
	return function(timeFraction) {
		return 1 - timing(1 - timeFraction);
	}
}

window.animateDefault = function (timeFraction) {
	return 1 - (1 - timeFraction)
}

window.animateCirc = function (timeFraction) {
	return 1 - Math.sin(Math.acos(timeFraction))
}

// x - 1,5
window.animateElastic = function (x, timeFraction) {
	return Math.pow(2, 10 * (timeFraction - 1)) * Math.cos(20 * Math.PI * x / 3 * timeFraction)
}

window.animateBounce = function (timeFraction) {
	for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
		if (timeFraction >= (7 - 4 * a) / 11) {
		return -Math.pow((11 - 6 * a - 11 * timeFraction) / 4, 2) + Math.pow(b, 2)
		}
	}
}

window.animateBounceEaseOut = animateMakeEaseOut(animateBounce);