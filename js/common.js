jQuery(document).ready(function () {
    // $(document).on('mouseup', '.js-ajax-remove,.js-ajax-removefromcart', function(){
    //
    //     // if ( settings.url === "/cart/index/index" ) {
    //     // }
    // });
    //spinner инпуты с увеличением и уменьшением
    jsInputSpinRefresh();
    //ajax удаление
    jQuery(this).on('click', ".js-ajax-remove", function () {
        var clicked = jQuery(this);
        var postparamname = jQuery(this).attr('postparamname');
        var names = postparamname.split(',');
        var data = {};
        jQuery.each(names, function (i, val) {
            data[val] = clicked.attr(val);
        });
        var url = jQuery(this).attr('url');
        var removeselector = jQuery(this).attr('removeselector');
        var quest = jQuery(this).attr('confirm');
        if (quest != 'false') {
            if (confirm(quest ? quest : 'Вы уверены?') == false)
                return false;
        }

        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {},
            success: function (data) {
                jQuery(removeselector).remove();

                if (typeof refreshTable == 'function')
                    refreshTable();

                //flashMessenger('success', ['Товар был удален из корзины']);
            }
        });
        return false;
    });

    //ajax массовое по галкам удаление
    jQuery(this).on('click', ".js-ajax-bulk-remove", function () {
        var clicked = jQuery(this);
        var checkboxselector = jQuery(this).attr('checkboxselector');
        var postparamname = jQuery(this).attr('postparamname');
        var addpostparamname = jQuery(this).attr('addpostparamname');
        var names = (addpostparamname === undefined ? [] : addpostparamname.split(','));
        var values = [];
        jQuery(checkboxselector+':checked').each(function(){
            values.push(jQuery(this).val());
        });
        var url = jQuery(this).attr('url');
        var removeselector = jQuery(this).attr('removeselector');
        var quest = jQuery(this).attr('confirm');

        if (confirm(quest ? quest : 'Вы уверены?') == false)
            return false;

        var data = {};
        data[postparamname] = values;
        jQuery.each(names, function (i, val) {
            data[val] = clicked.attr(val);
        });
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {},
            success: function (data) {
                $.each(values, function (index, value){
                    jQuery(removeselector+value).remove();
                });
                refreshTable();
            }
        });
    });

    //ajax подгрузка в контейнер
    jQuery(this).on('click', ".js-ajax-load-to-container", function (e) {
        var clicked = jQuery(this);
        var url = jQuery(this).attr('url');
        var containerselector = jQuery(this).attr('container');
        var postparamname = jQuery(this).attr('postparamname');
        var data = {};
        paginationAnchor($(this));
        if (postparamname != null) {
            var names = postparamname.split(',');
            jQuery.each(names, function (i, val) {
                data[val] = clicked.attr(val);
            });
        }
        jQuery(containerselector).html(loaderTemplate);
        $('#js-loader').height($('#js-loader').parent().height());


        jQuery.ajax({
            type: "GET",
            url: url,
            data: data,
            dataType: "html",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
            },
            success: function (data) {
                jQuery(containerselector).html(data);
                jsInputSpinRefresh();

                //console.log(jQuery(containerselector));
            }
        });
        return false;
        //e.stopPropagation();
    });

    //ajax подгрузка в контейнер v.2, city-lightbox
   jQuery(this).on('click', ".js-ajax-load-to-container2", function () {
       var clicked = jQuery(this);
       var postparamname = jQuery(this).data('postparamname');
       var value = jQuery(this).data(postparamname);
       var url = jQuery(this).data('url');
       var containerselector = jQuery(this).data('container');
       var data = {};
       data[postparamname] = value;
       if ($(this).data('target')){
           openLightbox($(this));
       }
       jQuery(containerselector).html(loaderTemplate);
       $('#js-loader').height($('#js-loader').parent().height());
       jQuery.ajax({
           type: "GET",
           url: url,
           data: data,
           dataType: "html",
           error: function (XMLHttpRequest, textStatus, errorThrown) {
               //statusMessage('Во время AJAX запроса произошла ошибка: '+textStatus+'!','show_img');
           },
           success: function (data) {
               jQuery(containerselector).html(data);
               jsInputSpinRefresh();
               cstmScroll();
           }
       });
       return false;
       //e.stopPropagation();
   });

    //dpd загрузка городов по региону
    jQuery(this).on('change', 'select[name=dpd_region_id]', function(){
        var dpd_region_id = jQuery(this).children('option:selected').val();
        //Гифка загрузки
        //jQuery('.js-ajax-load').html('<img src="/images/design/ajax-loader.gif"/>');
        jQuery.ajax({
            type: "POST",
            url: "/app/dpd/ajaxgetcities",
            dataType: "html",
            data: {
                dpd_region_id: dpd_region_id
            },
            success: function (html) {
                //удаление гифки загрузки
                //jQuery('.js-ajax-load').html('');
                jQuery('select[name=dpd_city_id]').html(html);
            }
        });
	});

    //датепикер диапазон дат
    jQuery('.js-datetimepicker-start-end').each(function () {
        var datepickerstart = jQuery(this).find('.datetimepicker-start').datetimepicker();
        var datepickerend = jQuery(this).find('.datetimepicker-end').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        datepickerstart.on("dp.change", function (e) {
            datepickerend.data("DateTimePicker").minDate(e.date);
        });
        datepickerend.on("dp.change", function (e) {
            datepickerstart.data("DateTimePicker").maxDate(e.date);
        });
    });
    
    jQuery('.js-datetimepicker').each(function () {
        var datepicker = jQuery(this).find('.datetimepicker').datetimepicker();
    });
    
    jQuery(this).on('click', '.js-confirm', function () {
        var quest = jQuery(this).attr('confirm');

        if (confirm(quest ? quest : 'Вы уверены?') == false)
            return false;
    });
});

// добавление классов &_focus, &_empty к textarea с нестандарт. скроллом
var inputClass = 'el-textinput__input', emptyClass = inputClass+'_empty', focusClass = inputClass+'_focus';

$(document).on('keyup focus blur','.scroll-wrapper.'+inputClass+'_textarea textarea',function(){setTextLabels(this)});
function setTextLabels(textar) {
    var $textar = $(textar), textarVal = $textar.val(), parent = $textar.closest('.scroll-wrapper');
    !(textarVal.length>0)?parent.addClass(emptyClass):parent.removeClass(emptyClass); //empty
    $textar.is(':focus')?parent.addClass(focusClass):parent.removeClass(focusClass); //focus
}

window.onload = function(){
    // Функция открывашек
    $(".b-delivery__toggle").click(function() {
        var e = $(this).closest(".b-delivery__item")
            ,t = e.find(".b-delivery__toggle .b-delivery__text")
            ,i = e.find(".b-delivery__details");
        e.hasClass("_active") ? i.slideUp(200, function() {
            e.removeClass("_active"),
            t.fadeIn(500)
        }) : (e.addClass("_active").siblings("._active").removeClass("_active"),
        e.siblings().find(".b-delivery__details").hide(),
        e.siblings().find(".b-delivery__toggle .b-delivery__text").show(),
        t.hide(),
        i.slideDown(500))
    });
    
    // // bonus steps function
    // var b_steps = '.b-info-bonus-steps'; //parent class
    // var b_steps__item = b_steps+'__item'; /* item */
    // var b_steps__item_regexp = new RegExp(b_steps__item.replace('.',''),'g'); //regexp
    // var b_steps__box = b_steps+'__box'; //box
    // //onhover
    // $(document).on('mouseenter',b_steps__item,function(){
    //     console.log('enter');
    //     var that = $(this);
    //     // ID элемента чей бокс нужно показать
    //     var thatID = that.attr('class').replace(b_steps__item_regexp,'').replace(' ','');
    //     // Родительский блок через который идет поиск элементов
    //     var parent = that.closest(b_steps);
    //     // Убираем/добавляем активный класс
    //     if (!parent.find(b_steps__box+thatID).hasClass(b_steps__box.replace('.','')+'_active')) {
    //         parent.find(b_steps__box).removeClass(b_steps__box.replace('.','')+'_active');
    //         parent.find(b_steps__box+thatID).addClass(b_steps__box.replace('.','')+'_active');
    //     };
    // });
}