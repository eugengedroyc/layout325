/**
 * Created by aleksander on 13.05.2019.
 */

//импорт
import { showFixHead, topLinePosition } from "./dev/main.js";

(function () {
    //event close menu $(document).trigger('close_main_menu');
    topLinePosition();
    $(window).scroll(function () {
        topLinePosition();
    });

    var btn = $(".js-but-catalog"),
        close = $('#js-close-nav'),
        bg_mask = $('#baground_big_block'),
        header = $('.header'),
        wrp_menu = $(".nav-catalogue_two li.nav-catalogue__item-wrap_haschild"),
        wrp_parent = $(".nav-catalogue__item-wrap_js_hover");

    btn.on('click', function () {
        if (header.hasClass('open')) {
            $(document).trigger('close_main_menu');
            topLinePosition();
        } else {
            header.addClass('open');
            bg_mask.show();
            topLinePosition();
        }
    });

    bg_mask.click(function() {
        $(document).trigger('close_main_menu');
    });

    close.click(function(e) {
        e.preventDefault();
        $(document).trigger('close_main_menu');
    });

    $(function () {
        $(document).on('close_main_menu',function () {
            header.removeClass('open');
            bg_mask.hide();
            $('html').css('overflow',"");
            topLinePosition();
        });
    });
})();


$(function () {
    var navLightbox = '.nav-lightbox',
	nav = '.nav-catalogue', // класс каталога
	nav_1 = nav + '_1 > .scrollbar-inner';

    $(nav_1).menuAim({
        firstActiveRow: true,
        rowSelector: "> span",
		activate: function(a) {
            let index =  $(a).index();
            $(a).addClass('js-lastopen');
            $('.nav-catalogue_two').children().eq(index).show();
		},
		deactivate: function(a) {
            $(a).removeClass('js-lastopen')
            $('.nav-catalogue_two').children().hide();
		}
    });
});
