# README #


### How do I get set up? ###

После стягивания репозитория:
1) Для установки всех node-modules и их зависимостей нужно запустить npm —save-dev install
   P.S. Рекомендуемая версия NodeJS не старше 8, если будут ошибки при установке
2) Из корня репозитория запустить таск для галпа: gulp AllGeks - запускает watchers для всего и копирует все CSS\JS файлы в папку с bitrix-проектом ..\3259404.local


### Contribution guidelines ###

HTML:
Шаблонизатор nunjucks. Компилируются в \html. Расположение: \templates
Nunjucks для повторяющихся блоков 

CSS:
Препроцессор LESS.  Компилируются в \css. Расположение: \less
Как и куда писать стили написано в \less\main.less

JS:
Препроцессор LESS.  Компилируются в \js. Расположение: \js\dev и модули \js
Сборщик webpack

Иконки:
SVG иконки можно внедрять с помощью SVG-injector https://www.npmjs.com/package/svg-injector-2

### Who do I talk to? ###

eugen.blizzart@gmail.com