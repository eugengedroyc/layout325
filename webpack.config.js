const path = require('path'),
			webpack = require('webpack'),
			MinifyPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
	mode : "development",
	entry: './js/dev/main.js',
	output: {
		path: path.resolve(__dirname, './'),
		filename: 'js/main.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: [{
					loader: 'babel-loader',
					query: {
						presets: [
							[
								'@babel/preset-env',
								{
									"modules": "false"
								}
							]
						]
					}
				}]
			},
			// {
      //   test: require.resolve('jquery'),
			// 	use: [{
			// 			loader: 'expose-loader',
			// 			options: 'jQuery'
			// 	},{
			// 			loader: 'expose-loader',
			// 			options: '$'
			// 	}]
    	// }
		]
	},
  plugins: [
    new MinifyPlugin()
  ],
	stats: {
		colors: true
	},
	// devtool: 'source-map',
	devServer: {
	  contentBase: './',
	  host: '0.0.0.0',
	  port: '8080',
	  stats: {
		colors: true,
		chunks: false
	  }
	}
};
